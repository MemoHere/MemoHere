package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.location.RecentItem;
import com.nexters.apeach.memohere.mapapi.GPSinfo;
import com.nexters.apeach.memohere.mapapi.MapApiConst;
import com.nexters.apeach.memohere.search.Item;
import com.nexters.apeach.memohere.search.OnFinishSearchListener;
import com.nexters.apeach.memohere.search.Searcher;

import java.lang.Runnable;

public class LocationActivity extends AppCompatActivity {

    private static final int REQ_CODE = 1000;
    public Button searchbt;
    public static EditText etlocation;
    public static ListView lvrecent = null;
    public static ListView poilist = null;
    public static TextView tvClear;

    public int itemId;

    public boolean  switchflag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Intent intent = getIntent();
        itemId = intent.getIntExtra("location_position", 0);

        final LocationAdapter lvadap;
        tvClear = (TextView) findViewById(R.id.tvClear);

        lvadap = new LocationAdapter();

        lvrecent = (ListView) findViewById(R.id.lvRecentSearch);
        lvrecent.setAdapter(lvadap);
        final POIAdapter poiAdapter = new POIAdapter();
        poilist = (ListView) findViewById(R.id.lvSearchPOI);
        poilist.setAdapter(poiAdapter);

        searchbt = (Button) findViewById(R.id.btnSearchLoca);
        etlocation = (EditText) findViewById(R.id.etSearchLoca);
        etlocation.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        searchbt.performClick();
                        break;
                    default:
                        // 기본 엔터키 동작
                        return false;
                }
                return true;
            }
        });
        final SharedPreferences spinit = getSharedPreferences("recent", MODE_PRIVATE);


        if (spinit.getInt("num", 0) == 0) {
            SharedPreferences.Editor editor = spinit.edit();
            editor.putInt("num", 0);
            editor.commit();
            tvClear.setVisibility(View.GONE);
        }

        for (int i = 1; i <= spinit.getInt("num", 0); i++) {
            lvadap.addItem(spinit.getString(String.valueOf(i), ""));
        }

        tvClear.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                SharedPreferences spclear = getSharedPreferences("recent", MODE_PRIVATE);
                SharedPreferences.Editor editor = spclear.edit();
                editor.putInt("num", 0);
                editor.commit();
                lvadap.clearItem();
                lvadap.notifyDataSetChanged();
                tvClear.setVisibility(View.GONE);
            }
        });

        lvrecent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long l) {
                RecentItem reitem = (RecentItem) parent.getItemAtPosition(pos);
                String recentname = reitem.getRecent();
                SharedPreferences sharedPreferences = getSharedPreferences("recent", MODE_PRIVATE);
                int recentnum = sharedPreferences.getInt("num", 0);

                for (int i = pos + 1; i < recentnum; i++) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString(String.valueOf(i), sharedPreferences.getString(String.valueOf(i + 1), ""));
                    edit.commit();
                }
                SharedPreferences.Editor edit = sharedPreferences.edit();
                recentnum--;
                edit.putInt("num", recentnum);
                edit.commit();
                lvadap.removeItem(pos);
                etlocation.setText(recentname);

                searchbt.performClick();
            }
        });

        searchbt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final SharedPreferences sprecent = getSharedPreferences("recent", MODE_PRIVATE);
                final int recentnum = sprecent.getInt("num", 0);
                String query = etlocation.getText().toString();

                if (query == null || query.length() == 0) {
                    Toast.makeText(LocationActivity.this, "검색어를 입력하세요.", Toast.LENGTH_SHORT).show();
                    return;
                }

                final String apikey = MapApiConst.DAUM_MAPS_ANDROID_APP_API_KEY;


                Searcher searcher = new Searcher();
                double latitude = 37.537229, longitude = 127.005515;
                GPSinfo gps = new GPSinfo(LocationActivity.this);
                if (gps.isGetLocation()) {

                    latitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                } else {
                    // GPS 를 사용할수 없으므로
                    gps.showSettingsAlert();
                }
                poilist = (ListView) findViewById(R.id.lvSearchPOI);
                poilist.setAdapter(poiAdapter);

                searcher.searchKeyword(getApplicationContext(), query, latitude, longitude, 1, apikey, new OnFinishSearchListener() {

                    @Override
                    public void onSuccess(final List<Item> itemList) {
                        int newnum;
                        if (itemList.size() == 0) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(LocationActivity.this, "검색 결과가 없습니다.", Toast.LENGTH_SHORT).show();
                                    etlocation.setText("");
                                }
                            });


                        } else {
                            for (int i = recentnum; i > 0; i--) {
                                SharedPreferences.Editor editor = sprecent.edit();
                                editor.putString(String.valueOf(i + 1), sprecent.getString(String.valueOf(i), ""));
                                editor.commit();
                            }
                            SharedPreferences.Editor editor = sprecent.edit();
                            editor.putString("1", etlocation.getText().toString());
                            editor.commit();

                            if (recentnum < 10) {
                                newnum = recentnum + 1;
                                editor.putInt("num", newnum);
                            } else {
                                newnum = 10;
                            }

                            editor.commit();
                            lvadap.clearItem();
                            for (int i = 1; i <= newnum; i++) {
                                lvadap.addItem(sprecent.getString(String.valueOf(i), ""));
                            }
                            poiAdapter.clearItemlist();
                            poiAdapter.addItemlist(itemList);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    poiAdapter.notifyDataSetChanged();
                                    lvadap.notifyDataSetChanged();
                                    lvrecent.setVisibility(View.GONE);
                                    tvClear.setVisibility(View.GONE);
                                    etlocation.setText("");
                                }
                            });

                        }
                    }

                    @Override
                    public void onFail() {
                        Toast.makeText(getApplicationContext(), "검색에 실패했습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

        });

        poilist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int pos, long l) {
                Item item = (Item) parent.getItemAtPosition(pos);
                Intent intent = new Intent(LocationActivity.this, MapplaceActivity.class);

                intent.putExtra("OBJECT",item);
                intent.putExtra("isReset",false);
                intent.putExtra("location_position",itemId);
                intent.putExtra("whatActivity",getIntent().getStringExtra("whatActivity"));
                intent.putExtra("switch",getIntent().getBooleanExtra("switch",false));
                startActivityForResult(intent, REQ_CODE);
            }
        });

        if (getIntent().getBooleanExtra("isReset", false)) {
            performMapplace();
        }

    }

    public void performMapplace() {
        Intent i = new Intent(LocationActivity.this,MapplaceActivity.class);
        i.putExtra("location_title",getIntent().getStringExtra("location_title"));
        i.putExtra("location_address",getIntent().getStringExtra("location_address"));
        i.putExtra("location_longitude",getIntent().getDoubleExtra("location_longitude",0));
        i.putExtra("location_latitude",getIntent().getDoubleExtra("location_latitude",0));
        i.putExtra("location_phone",getIntent().getStringExtra("location_phone"));
        i.putExtra("location_position",itemId);
        i.putExtra("location_destination",getIntent().getSerializableExtra("location_destination"));
        i.putExtra("isReset",true);
        i.putExtra("switch",getIntent().getBooleanExtra("switch",false));
        i.putExtra("whatActivity",getIntent().getStringExtra("whatActivity"));
        startActivityForResult(i,REQ_CODE);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (getIntent().getStringExtra("whatActivity").equals("add")) {
            Intent intent = new Intent(LocationActivity.this, AddDirectoryActivity.class); //if문 필요
            intent.putExtra("location_position",itemId);
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        } else if(getIntent().getStringExtra("whatActivity").equals("update")){
            Intent intent = new Intent(LocationActivity.this, UpdateDirectoryActivity.class); //if문 필요
            intent.putExtra("location_position",itemId);
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:

                    if (getIntent().getStringExtra("whatActivity").equals("add")) {
                        Intent intent = new Intent(LocationActivity.this, AddDirectoryActivity.class); //if문 필요
                        intent.putExtras(data);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                        break;
                    } else if(getIntent().getStringExtra("whatActivity").equals("update")) {
                        Intent intent = new Intent(LocationActivity.this, UpdateDirectoryActivity.class); //if문 필요
                        intent.putExtras(data);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                        break;
                    }
                    /*
                case Activity.RESULT_CANCELED :

                    if (getIntent().getStringExtra("whatActivity").equals("add")) {
                        Intent intent = new Intent(LocationActivity.this, AddDirectoryActivity.class); //if문 필요
                        intent.putExtras(data);
                        setResult(Activity.RESULT_CANCELED, intent);
                        finish();
                        break;
                    } else if(getIntent().getStringExtra("whatActivity").equals("update")) {
                        Intent intent = new Intent(LocationActivity.this, UpdateDirectoryActivity.class); //if문 필요
                        intent.putExtras(data);
                        setResult(Activity.RESULT_CANCELED, intent);
                        finish();
                        break;
                    }
                    */
            }
        }
    }
}



class LocationAdapter extends BaseAdapter {
    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<RecentItem> listViewItemList = new ArrayList<RecentItem>() ;

    // ListViewAdapter의 생성자
    public LocationAdapter() {

    }

    // Adapter에 사용되는 데이터의 개수를 리턴. : 필수 구현
    @Override
    public int getCount() {
        return listViewItemList.size() ;
    }

    // position에 위치한 데이터를 화면에 출력하는데 사용될 View를 리턴. : 필수 구현
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();


        // "listview_item" Layout을 inflate하여 convertView 참조 획득.
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_recentsearch, parent, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        final TextView recentTextView = (TextView) convertView.findViewById(R.id.tvRecentList);

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        RecentItem rcItem = listViewItemList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        recentTextView.setText(rcItem.getRecent());

        return convertView;
    }

    // 지정한 위치(position)에 있는 데이터와 관계된 아이템(row)의 ID를 리턴. : 필수 구현
    @Override
    public long getItemId(int position) {
        return position ;
    }

    // 지정한 위치(position)에 있는 데이터 리턴 : 필수 구현
    @Override
    public Object getItem(int position) {
        return listViewItemList.get(position) ;
    }

    // 아이템 데이터 추가를 위한 함수. 개발자가 원하는대로 작성 가능.
    public void addItem(String recent) {
        RecentItem item = new RecentItem();

        item.setRecent(recent);

        listViewItemList.add(item);
    }

    public void clearItem() {

        listViewItemList.clear();
    }

    public void removeItem(int i) {
        listViewItemList.remove(i);
    }

}

class POIAdapter extends BaseAdapter{


    public List<Item> Itemlist = new ArrayList<Item>();

    @Override
    public int getCount() {
        return Itemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return Itemlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_searchpoi,parent,false);
        }

        TextView tvTitle = (TextView)convertView.findViewById(R.id.tvSearchPOI);

        Item item = Itemlist.get(pos);

        tvTitle.setText(item.title);
        return convertView;
    }

    public void addItemlist(List<Item> inputresult){
        this.Itemlist.addAll(inputresult);
    }
    public void clearItemlist(){
        this.Itemlist.clear();
    }
}
