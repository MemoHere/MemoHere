package com.nexters.apeach.memohere.dto.memo;

import com.nexters.apeach.memohere.dto.Destination;
import com.nexters.apeach.memohere.search.Item;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by shin on 2017. 8. 6..
 */

public class AMemo {

    //default attribute
    private RootDirectory parent = null;
    private int id = -1;
    private MemoType memoType = MemoType.Normal;
    private String text = null;
    private Date createTime;
    private Date updateTime;

    private boolean alarmOff = false;
    public boolean onService = false;

    //option attribute
    private LocationOption locationOption = new LocationOption();
    private CategoryOption categoryOption = new CategoryOption();

    public AMemo(){

        this.createTime = Calendar.getInstance().getTime();
    }

    public RootDirectory getParent() {
        return parent;
    }

    public void setParent(RootDirectory parent) {
        this.parent = parent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MemoType getMemoType() {
        return memoType;
    }

    public void setMemoType(MemoType memoType) {
        this.memoType = memoType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isAlarmOff() {   return alarmOff;   }

    public int getAlarmOff() {return alarmOff?  1 : 0 ; }

    public void setAlarmOff(boolean alarmOff) { this.alarmOff = alarmOff;   }


    //option attribute
    public LocationOption getLocationOption() {
        return locationOption;
    }

    public void setLocationOption(LocationOption locationOption) {
        this.locationOption = locationOption;
    }

    public CategoryOption getCategoryOption() {
        return categoryOption;
    }

    public void setCategoryOption(CategoryOption categoryOption) {
        this.categoryOption = categoryOption;
    }

    public String getUserTitle(int length){

        String title = null;

        switch (memoType){
            case Location:
                title = locationOption.getTitle();
                break;
            case Category:
                title = categoryOption.getDetails();
                break;
        }

        if(title.length() > length){
            title = title.substring(0, length - 2) + "..";
        }

        return title;
    }

    public int getOptionRadius(){

        switch (memoType){
            case Location:
                return locationOption.getRadius();
            case Category:
                return categoryOption.getRadius();
        }

        return 0;
    }

    public List<Item> getOptionDestinationList(){

        Destination destination = null;

        switch (memoType){
            case Location:
                destination = locationOption.getDestination();
                break;
            case Category:
                destination = categoryOption.getDestination();
                break;
        }

        return destination != null? destination.getItemList() : null;
    }
}
