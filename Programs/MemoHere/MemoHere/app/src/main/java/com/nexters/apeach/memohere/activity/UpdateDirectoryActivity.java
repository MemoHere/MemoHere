package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Selection;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dto.Destination;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.CategoryOption;
import com.nexters.apeach.memohere.dto.memo.LocationOption;
import com.nexters.apeach.memohere.dto.memo.MemoType;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.search.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class UpdateDirectoryActivity extends AppCompatActivity {

    public EditText etDirectoryTitle;
    public Button btnSaveDirectory;
    public ImageButton btnAddMemo;
    public ImageButton btnDeleteMemo;
    public ListView lvMemo;
    public MemoAdapter memoAdapter;

    DataManager dataManager;
    RootDirectory thisDirectory;
    List<AMemo> memoList;
    int pos;

    private CustomDialog mCustomDialog;

    private static final int REQ_LOCATION_CODE = 1000;
    private static final int REQ_CATEGORY_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_directory);

        Intent intent = getIntent();
        pos = (int) intent.getSerializableExtra("update_data");

        dataManager = DataManager.createInstance(this);
        thisDirectory = dataManager.getDirectory(pos);
        memoList = thisDirectory.getMemoList();

        etDirectoryTitle = (EditText) findViewById(R.id.etDirectoryTitle);
        etDirectoryTitle.setTextColor(Color.BLACK);
        etDirectoryTitle.setText(thisDirectory.getTitle());
        etDirectoryTitle.setSelection(etDirectoryTitle.getText().length());

        memoAdapter = new MemoAdapter(this, R.layout.listview_memo, memoList, new MemoAdapter.LocationBtnClickListener() {
            @Override
            public void onLocationClick(int position, ToggleButton btnLo) {
                boolean isCheck = btnLo.isChecked();
                Intent i = new Intent(UpdateDirectoryActivity.this, LocationActivity.class);
                if(memoList.get(position).getLocationOption().getTitle()!=null) { // 위치 메모가 이미 설정 되어 있을 때
                    btnLo.setChecked(true);
                    LocationOption locationOption = memoList.get(position).getLocationOption();
                    i.putExtra("location_title",locationOption.getTitle());
                    i.putExtra("location_address",locationOption.getAddress());
                    i.putExtra("location_latitude", locationOption.getLatitude());
                    i.putExtra("location_longitude",locationOption.getLongitude());
                    i.putExtra("location_phone",locationOption.getPhone());
                    i.putExtra("location_destination",locationOption.getDestination());
                    i.putExtra("switch",memoList.get(position).isAlarmOff());
                    i.putExtra("isReset",true);
                }

                else {
                    LocationOption locationOption = new LocationOption();
                    i.putExtra("isReset",false);
                }

                i.putExtra("whatActivity", "update");
                i.putExtra("location_position", position);

                if(memoList.get(position).getCategoryOption().getName()!=null) {
                    final int pos = position;
                    final Intent newIn = i;

                    View.OnClickListener leftListener = new View.OnClickListener() {
                        public void onClick(View v) {

                            CategoryOption categoryOption = new CategoryOption();
                            memoList.get(pos).setMemoType(MemoType.Location);
                            memoList.get(pos).setCategoryOption(categoryOption);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });

                            startActivityForResult(newIn, REQ_LOCATION_CODE);

                            mCustomDialog.dismiss();
                        }
                    };

                    View.OnClickListener rightListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            mCustomDialog.dismiss();
                        }
                    };

                    mCustomDialog = new CustomDialog(UpdateDirectoryActivity.this,
                            "위치 메모로 변경하시겠습니까?\n기존 설정은 사라집니다.", // 내용
                            leftListener, // 왼쪽 버튼 이벤트
                            rightListener); // 오른쪽 버튼 이벤트
                    mCustomDialog.show();
                }

                else {
                    memoList.get(position).setMemoType(MemoType.Location);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });
                    startActivityForResult(i, REQ_LOCATION_CODE);
                }

            }
        }, new MemoAdapter.CategoryBtnClickListener() {
            @Override
            public void onCategoryClick(int position, ToggleButton btnCa) {
                boolean isCheck = btnCa.isChecked();
                Intent i = new Intent(UpdateDirectoryActivity.this, CategoryActivity.class);
                if (memoList.get(position).getCategoryOption().getName()!=null) {
                    btnCa.setChecked(true);
                    CategoryOption categoryOption = memoList.get(position).getCategoryOption();
                    i.putExtra("category_name", categoryOption.getName());
                    i.putExtra("category_detail", categoryOption.getDetails());
                    i.putExtra("category_radius", categoryOption.getRadius());
                    i.putExtra("switch",memoList.get(position).isAlarmOff());
                    i.setAction(Long.toString(SystemClock.currentThreadTimeMillis()));
                }
                else {
                    CategoryOption categoryOption = new CategoryOption();
                }

                i.putExtra("whatActivity","update");
                i.putExtra("category_position", position);

                if (memoList.get(position).getLocationOption().getTitle()!=null) {
                    final int pos = position;
                    final Intent newIn = i;

                    View.OnClickListener leftListener = new View.OnClickListener() {
                        public void onClick(View v) {

                            LocationOption locationOption = new LocationOption();
                            memoList.get(pos).setMemoType(MemoType.Category);
                            memoList.get(pos).setLocationOption(locationOption);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });

                            startActivityForResult(newIn, REQ_CATEGORY_CODE);

                            mCustomDialog.dismiss();
                        }
                    };

                    View.OnClickListener rightListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            mCustomDialog.dismiss();
                        }
                    };

                    mCustomDialog = new CustomDialog(UpdateDirectoryActivity.this,
                            "카테고리 메모로 변경하시겠습니까?\n기존 설정은 사라집니다.", // 내용
                            leftListener, // 왼쪽 버튼 이벤트
                            rightListener); // 오른쪽 버튼 이벤트
                    mCustomDialog.show();
                }

                // 위치 메모가 설정 되어 있지 않을 때, 카테고리 메모 설정 시
                else {
                    memoList.get(position).setMemoType(MemoType.Category);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });

                    startActivityForResult(i, REQ_CATEGORY_CODE);
                }
            }
        });


        lvMemo = (ListView) findViewById(R.id.lvMemo);
        lvMemo.setAdapter(memoAdapter);
        lvMemo.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                if (!AddDirectoryActivity.isDeleteMode) {
                    AddDirectoryActivity.isDeleteMode = true;
                    btnDeleteMemo.setVisibility(View.VISIBLE);
                    btnSaveDirectory.setVisibility(View.GONE);
                    btnAddMemo.setVisibility(View.GONE);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });
                    return true;
                } else
                    return false;
            }
        });

        btnDeleteMemo = (ImageButton) findViewById(R.id.btnDeleteMemo);
        btnDeleteMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<Integer, Boolean> delMap = memoAdapter.getCheckedList();
                final List<AMemo> newMemoList = new ArrayList<AMemo>();

                for (int i = 0; i < memoList.size(); i++) {
                    if (!delMap.get(i)) {
                        newMemoList.add(memoList.get(i));
                    }
                }

                View.OnClickListener leftListener = new View.OnClickListener() {
                    public void onClick(View v) {

                        if (newMemoList.size() == 0) { // 메모 전부 다 삭제 할 때

                            AMemo memo = new AMemo();

                            memoList.clear();
                            memoList.add(memo);

                            AddDirectoryActivity.isDeleteMode = false;
                            btnSaveDirectory.setVisibility(View.VISIBLE);
                            btnDeleteMemo.setVisibility(View.GONE);
                            btnAddMemo.setVisibility(View.VISIBLE);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });

                        } else {

                            memoList.clear();
                            memoList.addAll(newMemoList);

                            AddDirectoryActivity.isDeleteMode = false;
                            btnSaveDirectory.setVisibility(View.VISIBLE);
                            btnDeleteMemo.setVisibility(View.GONE);
                            btnAddMemo.setVisibility(View.VISIBLE);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });
                        }

                        mCustomDialog.dismiss();
                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    public void onClick(View v) {
                        mCustomDialog.dismiss();
                    }
                };

                mCustomDialog = new CustomDialog(UpdateDirectoryActivity.this,
                        "메모를 삭제하시겠습니까?", // 내용
                        leftListener, // 왼쪽 버튼 이벤트
                        rightListener); // 오른쪽 버튼 이벤트
                mCustomDialog.show();
            }
        });

        btnAddMemo = (ImageButton) findViewById(R.id.btnAddMemo);
        btnAddMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (memoList.get(memoList.size() - 1).getText() == null||memoList.get(memoList.size() - 1).getText().equals("")) { // 작성 중인 메모 내용 비어있으면 추가 못함
                    Toast toast = Toast.makeText(UpdateDirectoryActivity.this, "작성 중인 메모가 있습니다.", Toast.LENGTH_SHORT);
                    toast.show();

                } else {
                    AMemo memo = new AMemo();
                    ArrayList<Integer> idList = new ArrayList<Integer>();

                    for (int i = 0; i < memoList.size(); i++) {
                        idList.add(memoList.get(i).getId());
                    }

                    Integer maxId = Collections.max(idList);

                    memo.setId(maxId + 1);
                    memo.setText("");
                    memoList.add(memo);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

        btnSaveDirectory = (Button) findViewById(R.id.btnSaveDirectory);
        btnSaveDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (memoList.size() == 1 && (memoList.get(0).getText() == null || memoList.get(0).getText().equals(""))) {

                    Toast toast = Toast.makeText(UpdateDirectoryActivity.this, "저장된 내용이 없어 메모가 삭제됩니다.", Toast.LENGTH_SHORT);
                    toast.show();

                    Intent i = new Intent(UpdateDirectoryActivity.this, MainActivity.class);
                    i.putExtra("delete_memo", thisDirectory.getId());
                    setResult(Activity.RESULT_OK, i);
                    finish();

                } else {

                    RootDirectory tmpDir = new RootDirectory();
                    tmpDir.setCreateTime(thisDirectory.getCreateTime());
                    if (etDirectoryTitle.getText().toString().equals("")) {
                        tmpDir.setTitle(memoList.get(0).getText());
                    } else tmpDir.setTitle(etDirectoryTitle.getText().toString());
                    tmpDir.setId(thisDirectory.getId());

                    for (int i = 0; i < memoList.size(); i++) {

                        AMemo memo = memoList.get(i);

                        if (memoList.get(i).getText() != null && memoList.get(i).getText().length() != 0)
                            tmpDir.addMemo(memo);
                    }

                    dataManager.updateDirectory(tmpDir);

                    Intent i = new Intent(UpdateDirectoryActivity.this, MainActivity.class);
                    i.putExtra("updateDir", tmpDir.getId());
                    Toast.makeText(UpdateDirectoryActivity.this, "메모가 저장되었습니다.", Toast.LENGTH_LONG).show();
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_LOCATION_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:

                    LocationOption location = new LocationOption();
                    location.setTitle(data.getStringExtra("location_title"));
                    location.setAddress(data.getStringExtra("location_address"));
                    location.setLongitude(data.getDoubleExtra("location_longitude",0));
                    location.setLatitude(data.getDoubleExtra("location_latitude",0));
                    location.setPhone(data.getStringExtra("location_phone"));

                    if(!data.getBooleanExtra("isReset",false)) {
                        Destination des = new Destination();
                        ArrayList<Item> desList = new ArrayList<>();
                        desList.add((Item) data.getSerializableExtra("OBJECT"));
                        des.setDestinationList(desList);
                        location.setDestination(des);
                    }

                    else {
                        Destination des = (Destination) data.getSerializableExtra("location_destination");
                        location.setDestination(des);
                    }

                    memoList.get(data.getIntExtra("location_position",0)).setAlarmOff(data.getBooleanExtra("switch",false));
                    if(memoList.get(data.getIntExtra("location_position",0)).isAlarmOff()) {
                        memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Normal);
                    }
                    else {
                        memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Location);
                    }
                    memoList.get(data.getIntExtra("location_position",0)).setLocationOption(location);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });

                    break;

                case Activity.RESULT_CANCELED :

                    if(memoList.get(data.getIntExtra("location_position",0)).getLocationOption().getTitle() == null)
                    {
                        memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Normal);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                memoAdapter.notifyDataSetChanged();
                            }
                        });
                    }

//                    LocationOption locationNullOption = new LocationOption();
//
//                    memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Normal);
//                    memoList.get(data.getIntExtra("category_position",0)).setLocationOption(locationNullOption);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            memoAdapter.notifyDataSetChanged();
//                        }
//                    });
            }
        }

        else if(requestCode == REQ_CATEGORY_CODE) {

            switch (resultCode) {
                case Activity.RESULT_OK:

                    CategoryOption categoryOption = new CategoryOption();
                    categoryOption.setName(data.getStringExtra("category_name"));
                    categoryOption.setDetails(data.getStringExtra("category_detail"));
//                    categoryOption.setRadius(data.getDoubleExtra("category_radius",0));
                    memoList.get(data.getIntExtra("category_position",0)).setAlarmOff(data.getBooleanExtra("switch",false));
                    memoList.get(data.getIntExtra("category_position",0)).setCategoryOption(categoryOption);
                    if(memoList.get(data.getIntExtra("category_position",0)).isAlarmOff()) {
                        memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Normal);
                    }
                    else {
                        memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Category);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });

                    break;

                case Activity.RESULT_CANCELED :

                    if(memoList.get(data.getIntExtra("category_position",0)).getCategoryOption().getName() == null)
                    {
                        memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Normal);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                memoAdapter.notifyDataSetChanged();
                            }
                        });
                    }

//                    CategoryOption categoryNullOption = new CategoryOption();
//
//                    memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Normal);
//                    memoList.get(data.getIntExtra("category_position",0)).setCategoryOption(categoryNullOption);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            memoAdapter.notifyDataSetChanged();
//                        }
//                    });
            }
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (!AddDirectoryActivity.isDeleteMode) {

            if (memoList.size() == 1 && (memoList.get(0).getText() == null || memoList.get(0).getText().equals(""))) {

                Toast toast = Toast.makeText(UpdateDirectoryActivity.this, "저장된 내용이 없어 메모가 삭제됩니다.", Toast.LENGTH_SHORT);
                toast.show();

                Intent i = new Intent(UpdateDirectoryActivity.this, MainActivity.class);
                i.putExtra("delete_memo", thisDirectory.getId());
                setResult(Activity.RESULT_OK, i);
                finish();

            } else {

                RootDirectory tmpDir = new RootDirectory();
                tmpDir.setCreateTime(thisDirectory.getCreateTime());
                if (etDirectoryTitle.getText().toString().equals("")) {
                    tmpDir.setTitle(memoList.get(0).getText());
                } else tmpDir.setTitle(etDirectoryTitle.getText().toString());
                tmpDir.setId(thisDirectory.getId());

                for (int i = 0; i < memoList.size(); i++) {

                    AMemo memo = memoList.get(i);

                    if (memoList.get(i).getText() != null && memoList.get(i).getText().length() != 0)
                        tmpDir.addMemo(memo);
                }

                dataManager.updateDirectory(tmpDir);

                Intent i = new Intent(UpdateDirectoryActivity.this, MainActivity.class);
                i.putExtra("updateDir", tmpDir.getId());
                Toast.makeText(UpdateDirectoryActivity.this, "메모가 저장되었습니다.", Toast.LENGTH_LONG).show();
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        }

        else {
            AddDirectoryActivity.isDeleteMode = false;
            btnSaveDirectory.setVisibility(View.VISIBLE);
            btnDeleteMemo.setVisibility(View.GONE);
            btnAddMemo.setVisibility(View.VISIBLE);
        }
    }
}