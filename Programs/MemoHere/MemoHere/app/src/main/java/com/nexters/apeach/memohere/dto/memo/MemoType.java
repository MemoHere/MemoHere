package com.nexters.apeach.memohere.dto.memo;

/**
 * Created by shin on 2017. 7. 25..
 */

public enum MemoType {

    Normal,
    Location,
    Category
}
