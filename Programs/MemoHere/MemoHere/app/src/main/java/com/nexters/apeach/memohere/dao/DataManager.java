package com.nexters.apeach.memohere.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nexters.apeach.memohere.dto.Destination;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.MemoType;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.search.Item;
import com.nexters.apeach.memohere.search.OnFinishSearchListener;
import com.nexters.apeach.memohere.search.Searcher;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Soyeon on 2017-07-17.
 */

public class DataManager {

//    private final String DB_PATH = null;
    private DBHelper dbHelper;
    private HashMap<Integer, RootDirectory> dirMap;

    private static DataManager instance;
    public static DataManager createInstance(Context context){

        if(instance == null){
            instance = new DataManager(context);
        }
        return instance;
    }


    private DataManager(Context context) {
        dbHelper = new DBHelper(context);
        dirMap = new HashMap<Integer, RootDirectory>();

        if(!load()){
            //fail to read database
            Log.d(this.toString(), "fail to read database");
        }
        else{

            //임시로 destination 생성, 이후 로케이션 옵션은 destination 제거
            for (RootDirectory dir: dirMap.values()) {
                for (final AMemo memo: dir.getMemoList()) {
                    if(memo.getMemoType() == MemoType.Location){
                        Searcher searcher = new Searcher();
                        searcher.searchDestination(context,
                                memo.getLocationOption().getTitle(),
                                memo.getLocationOption().getLatitude(),
                                memo.getLocationOption().getLongitude(),
                                1, 1,
                                new OnFinishSearchListener() {
                                    @Override
                                    public void onSuccess(List<Item> itemList) {
                                        Destination destination = new Destination();
                                        destination.setDestinationList(itemList);

                                        memo.getLocationOption().setDestination(destination);
                                    }

                                    @Override
                                    public void onFail() {

                                    }
                                });
                    }
                }
            }
        }
    }

    private boolean load(){

        try {
            //load directory
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor c = db.rawQuery("select * from " + dbHelper.DIRECTORY_TABLE_NAME , null);
            while (c.moveToNext()) {
                RootDirectory tmp = new RootDirectory();
                tmp.setId(c.getInt(c.getColumnIndex("_ID")));
                tmp.setTitle(c.getString(c.getColumnIndex("TITLE")));
                try {
                    tmp.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(c.getString(c.getColumnIndex("CREATIONTIME"))));

                    if(c.getString(c.getColumnIndex("UPDATETIME")) != null)
                    tmp.setUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(c.getString(c.getColumnIndex("UPDATETIME"))));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(c.getInt(c.getColumnIndex("DELETED")) == 1){ tmp.setDeleted(true); }
                else{   tmp.setDeleted(false);   }
                dirMap.put(tmp.getId(), tmp);
            }

            //load memo
            c = db.rawQuery("select * from " + dbHelper.MEMO_TABLE_NAME, null);

            /*
            * ( _ID integer primary key autoincrement, ROOTID integer, MEMOTYPE text ,TEXT text, CREATIONTIME datetime, UPDATETIME datetime," +
                          " CATEGORYNAME text, CATEGORYDETAILED text, CATEGORYRADIUS double" +
                                ", ADDRESS text, LON double, LAT double, LOCATIONRADIUS double  );";
             */

            while (c.moveToNext()) {

                AMemo tmpMemo = new AMemo();
                tmpMemo.setId(c.getInt(c.getColumnIndex("_ID")));
                tmpMemo.setText(c.getString(c.getColumnIndex("TEXT")));
                tmpMemo.setMemoType(MemoType.valueOf(c.getString(c.getColumnIndex("MEMOTYPE"))));

                tmpMemo.setCreateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(c.getString(c.getColumnIndex("CREATIONTIME"))));

                if(c.getString(c.getColumnIndex("UPDATETIME")) != null)
                tmpMemo.setUpdateTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(c.getString(c.getColumnIndex("UPDATETIME"))));

                if(c.getInt(c.getColumnIndex("ALARMOFF")) == 1){ tmpMemo.setAlarmOff(true); }
                Log.i("확인", tmpMemo.getAlarmOff()+"");

                //카테고리 메모일 때
                if(tmpMemo.getMemoType().equals(MemoType.Category)) {
                    tmpMemo.getCategoryOption().setName(c.getString(c.getColumnIndex("CATEGORYNAME")));
                    tmpMemo.getCategoryOption().setDetails(c.getString(c.getColumnIndex("CATEGORYDETAILED")));
//                    tmpMemo.getCategoryOption().setRadius(c.getDouble(c.getColumnIndex("CATEGORYRADIUS")));
                }
                //위치 지정 메모일 때
                if(tmpMemo.getMemoType().equals(MemoType.Location)) {
                    tmpMemo.getLocationOption().setTitle(c.getString(c.getColumnIndex("TITLE")));
                    tmpMemo.getLocationOption().setAddress(c.getString(c.getColumnIndex("ADDRESS")));
                    tmpMemo.getLocationOption().setLongitude(c.getDouble(c.getColumnIndex("LON")));
                    tmpMemo.getLocationOption().setLatitude(c.getDouble(c.getColumnIndex("LAT")));
                    tmpMemo.getLocationOption().setPhone(c.getString(c.getColumnIndex("PHONE")));
                    tmpMemo.getLocationOption().setRadius(c.getDouble(c.getColumnIndex("LOCATIONRADIUS")));
                    Log.i("확인", tmpMemo.getLocationOption().getTitle() +"/"+ tmpMemo.getLocationOption().getAddress() );
                }

                int rootId = c.getInt(c.getColumnIndex("ROOTID"));
                RootDirectory dir = dirMap.get(rootId);
                dir.addMemo(tmpMemo);
            }
            c.close();
            db.close();
        }
        catch (Exception e){
            e.getStackTrace();
            return false;
        }

        return true;
    }

    public void close(){ dbHelper.close();}

    //handle with directory
    public void addDirectory(RootDirectory dir){
        
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction(); //트랜잭션처리
        int id = 0;
        try {
        // DIRECTORY 생성
        db.execSQL("insert into " +dbHelper.DIRECTORY_TABLE_NAME+ "(TITLE, CREATIONTIME) values('" + dir.getTitle() + "' , '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dir.getCreateTime()) +"');");
        Cursor c = db.rawQuery("select last_insert_rowid() from "+ dbHelper.DIRECTORY_TABLE_NAME, null);

        while(c.moveToNext()) {id = c.getInt(0);}
        dir.setId(id);

        //MEMOTYPE 에 따라서 insert문 작성
        List<AMemo> tempMemoList = new ArrayList<>();
        for(AMemo memo :dir.getMemoList()){
            db.execSQL("insert into " +dbHelper.MEMO_TABLE_NAME+ "(ROOTID, MEMOTYPE, TEXT, CREATIONTIME, ALARMOFF, CATEGORYNAME, CATEGORYDETAILED, CATEGORYRADIUS, TITLE, ADDRESS, LON,  LAT, PHONE, LOCATIONRADIUS) values('"
                    + id + "' , '"
                    + memo.getMemoType() + "' , '"
                    + memo.getText() + "' , '"
                    + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dir.getCreateTime()) + "' , '"
                    + memo.getAlarmOff() + "' , '"
                    + memo.getCategoryOption().getName() + "' , '"
                    + memo.getCategoryOption().getDetails() + "' , '"
                    + memo.getCategoryOption().getRadius() + "' , '"
                    + memo.getLocationOption().getTitle() + "' , '"
                    + memo.getLocationOption().getAddress() + "' , '"
                    + memo.getLocationOption().getLongitude() + "' , '"
                    + memo.getLocationOption().getLatitude() + "' , '"
                    + memo.getLocationOption().getPhone() + "' , '"
                    + memo.getLocationOption().getRadius() + "');");

            c = db.rawQuery("select last_insert_rowid() from "+ dbHelper.MEMO_TABLE_NAME, null);
            if(c.moveToNext()) {memo.setId(c.getInt(0));}

            tempMemoList.add(memo);
        }
            c.close();
            db.setTransactionSuccessful();

            dir.clear();
            for (AMemo m: tempMemoList) {
                dir.addMemo(m);
            }
        }catch (SQLException e){
            Log.e("확인 :  datamanager_add", e.toString());
        }
        finally {
            // HASHMAP에 추가
            dirMap.put(id, dir);
           db.endTransaction();
        }
        db.close();
    }

    //영구삭제
    public void removeDirectory(RootDirectory dir){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            //MEMO DELETE
            db.execSQL("delete from " + dbHelper.MEMO_TABLE_NAME + " where ROOTID = " + dir.getId() + ";");
            //DIRECTORY DELETE
            db.execSQL("delete from " + dbHelper.DIRECTORY_TABLE_NAME + " where _id = " + dir.getId() + ";");

            db.setTransactionSuccessful();
        }catch (SQLException e){
            Log.e("확인 :  datamanager_removeDir", e.toString());
        }
        finally {
            db.endTransaction();
        }
        db.close();

        dirMap.remove(dir.getId());
    }

    public void removeDirectoryALL(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ArrayList<Integer> idList = new ArrayList<Integer>();

        db.beginTransaction();
        try {
            for (RootDirectory dir : dirMap.values()) {
                if (dir.isDeleted()) {
                    //MEMO DELETE
                    db.execSQL("delete from " + dbHelper.MEMO_TABLE_NAME + " where ROOTID = " + dir.getId() + ";");
                    idList.add(dir.getId());
                }
            }
            //DIRECTORY DELETE
            db.execSQL("delete from " + dbHelper.DIRECTORY_TABLE_NAME + " where DELETED = 1;");

            db.setTransactionSuccessful();
        }catch (SQLException e){
            Log.e("확인 :  datamanager_removeAll", e.toString());
        }
        finally {
            db.endTransaction();
        }
        for(int id : idList){
            if(dirMap.get(id).isDeleted()){
                dirMap.remove(id);
            }
        }
        db.close();


    }

    public void updateDirectory(RootDirectory dir){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        dir.setUpdateTime(new Date());
        db.beginTransaction();
        try{
        //DIRECTORY의 UPDATETIME과 TITLE 변경
        db.execSQL("update "+dbHelper.DIRECTORY_TABLE_NAME+" set UPDATETIME = '"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dir.getUpdateTime())+"', TITLE = '"+ dir.getTitle()+"' where _id = '"+ dir.getId()+"' ;");

        //매개변수로 들어온 디렉토리와와 기존의 디레토리의 id를 비교하여 처리
        for(AMemo memo : dir.getMemoList()){
            if(dirMap.get(dir.getId()).containsKeyMemo(memo.getId())){ // 이미 Id가 존재하는 경우-> 수정
            db.execSQL("update "+dbHelper.MEMO_TABLE_NAME+" set UPDATETIME = '"+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dir.getUpdateTime())+"', MEMOTYPE = '"+ memo.getMemoType()+"', TEXT= '"+ memo.getText()+"', ALARMOFF= '"+memo.getAlarmOff()
                    +"', CATEGORYNAME= '"+ memo.getCategoryOption().getName()+"', CATEGORYDETAILED= '"+ memo.getCategoryOption().getDetails()+"', CATEGORYRADIUS= '"+ memo.getCategoryOption().getRadius()
                    +"', TITLE= '"+ memo.getLocationOption().getTitle()+"', ADDRESS= '"+ memo.getLocationOption().getAddress()+"', LON= '"+ memo.getLocationOption().getLongitude()+"', LAT= '"+ memo.getLocationOption().getLatitude()+"', PHONE= '"+ memo.getLocationOption().getPhone()+"', LOCATIONRADIUS= '"+ memo.getLocationOption().getRadius()
                    +"' where _id = "+ memo.getId()+";");
            }

            else if(memo.getCreateTime() == null){ //새로 추가 된 경우
                db.execSQL("insert into " +dbHelper.MEMO_TABLE_NAME+ "(ROOTID, MEMOTYPE, TEXT, CREATIONTIME, ALARMOFF, CATEGORYNAME, CATEGORYDETAILED, CATEGORYRADIUS, TITLE, ADDRESS, LON,  LAT, PHONE, LOCATIONRADIUS) values('"
                        + dir.getId() + "' , '"
                        + memo.getMemoType() + "' , '"
                        + memo.getText() + "' , '"
                        + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dir.getUpdateTime()) + "' , '"
                        + memo.getAlarmOff() + "' , '"
                        + memo.getCategoryOption().getName() + "' , '"
                        + memo.getCategoryOption().getDetails()+ "' , '"
                        + memo.getCategoryOption().getRadius() + "' , '"
                        + memo.getLocationOption().getTitle()+ "' , '"
                        + memo.getLocationOption().getAddress()+ "' , '"
                        + memo.getLocationOption().getLongitude() + "' , '"
                        + memo.getLocationOption().getLatitude()+ "' , '"
                        + memo.getLocationOption().getPhone()+ "' , '"
                        + memo.getLocationOption().getRadius() + "');");
            }
        }

        //기존 디렉토리에는 존재하고 새로운 디렉토리에 존재하지 않는 ID를 가진 메모는 삭제
        for(AMemo memo : dirMap.get(dir.getId()).getMemoList()){
            if(!dir.containsKeyMemo(memo.getId()))
                db.execSQL("delete from " + dbHelper.MEMO_TABLE_NAME + " where _ID = " + memo.getId() + ";");
        }
        db.setTransactionSuccessful();
        }catch (SQLException e){
            Log.e("확인 :  datamanager_Update", e.toString());

        }finally {
            db.endTransaction();
        }
        db.close();
        //dirMap.remove(dir.getId());
        dirMap.put(dir.getId(), dir);

    }

    //쓰레기통으로이동 set Deleted = 1;
    public void moveTrashDirectory(RootDirectory dir){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            db.execSQL("update " + dbHelper.DIRECTORY_TABLE_NAME + " set DELETED = 1 where _id = '" + dir.getId() + "' ;");
            db.setTransactionSuccessful();
        }catch (SQLException e ){
            Log.e("확인 :  datamanager_MoveTrash", e.toString());
        }finally {
            db.endTransaction();
        }
        dir.delete();
        db.close();

        dirMap.put(dir.getId(), dir);
    }

    //쓰레기통에서 복원 set Deleted = 0;
    public void retunrTrashDIrectory(RootDirectory dir){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            db.execSQL("update " + dbHelper.DIRECTORY_TABLE_NAME + " set DELETED = 0 where _id = '" + dir.getId() + "' ;");
            db.setTransactionSuccessful();
        }catch (SQLException e){
            Log.e("확인 :  datamanager_ReturnTrash", e.toString());
        }finally {
            db.endTransaction();
        }
        dir.setDeleted(false);
        db.close();

        dirMap.put(dir.getId(), dir);
    }
    //휴지통에 보여줄 dirList
    public ArrayList<RootDirectory> getTrashList(){
        ArrayList<RootDirectory> tmp = new ArrayList<RootDirectory>();

        for(RootDirectory dir : dirMap.values()){
            if(dir.isDeleted()) tmp.add(dir);
        }
        return tmp;
    }

    public ArrayList<RootDirectory> getMainList(){
        ArrayList<RootDirectory> tmp = new ArrayList<RootDirectory>();

        for(RootDirectory dir : dirMap.values()){
            if(!dir.isDeleted()) tmp.add(dir);
        }
        return tmp;
    }

    public RootDirectory getDirectory(int id){

        if(this.dirMap.containsKey(id))
            return this.dirMap.get(id);

        return null;
    }

    public List<RootDirectory> getDirectories(){
        return new ArrayList<RootDirectory>(this.dirMap.values());
    }

    public void setAlarm(int id, boolean flag){
        int alarm = flag ? 1 : 0;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            db.execSQL("update " + dbHelper.MEMO_TABLE_NAME + " set ALARMOFF = '"+alarm+"' where _id = '" + id + "' ;");
            db.setTransactionSuccessful();
        }catch (SQLException e ){
            Log.e("확인 :  datamanager_setAlarm", e.toString());
        }finally {
            db.endTransaction();
        }
        db.close();
    }

    public void setAlarmAll(boolean flag){
        int alarm = flag ? 1 : 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            db.execSQL("update " + dbHelper.MEMO_TABLE_NAME + " set ALARMOFF = '"+alarm+ "' ;");
            db.setTransactionSuccessful();
        }catch (SQLException e ){
            Log.e("확인 :  datamanager_setAlarmAll", e.toString());
        }finally {
            db.endTransaction();
        }
        db.close();
    }

}
