package com.nexters.apeach.memohere.processor.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * Created by shin on 2017. 8. 15..
 */

public class AutoServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        /* 서비스 죽일때 알람으로 다시 서비스 등록 */
        if (intent.getAction().equals("ACTION.RESTART.BackService")) {

            Intent i = new Intent(context, BackService.class);
            context.startService(i);
        }

        /* 폰 재부팅할때 서비스 등록 */
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {

            SharedPreferences sp = context.getSharedPreferences("Noti", context.MODE_PRIVATE);
            if(sp.getBoolean("AlarmOnOff",true)) {

                Intent i = new Intent(context, BackService.class);
                context.startService(i);
            }
        }
    }
}
