package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.dto.Destination;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dto.memo.CategoryOption;
import com.nexters.apeach.memohere.dto.memo.LocationOption;
import com.nexters.apeach.memohere.dto.memo.MemoType;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.search.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddDirectoryActivity extends AppCompatActivity {

    public EditText etDirectoryTitle;
    public Button btnSaveDirectory;
    public ImageButton btnAddMemo;
    public ImageButton btnDeleteMemo;
    public ListView lvMemo;
    public MemoAdapter memoAdapter;
    static boolean isDeleteMode;

    DataManager dataManager;
    List<AMemo> memoList;

    private CustomDialog mCustomDialog;

    private static final int REQ_LOCATION_CODE = 1000;
    private static final int REQ_CATEGORY_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_directory);

        isDeleteMode = false;

        // 메모 리스트 생성 및 빈 메모 객체 하나 생성
        dataManager = DataManager.createInstance(this);
        memoList = new ArrayList<AMemo>();
        final AMemo memo = new AMemo();
        memoList.add(memo);

        etDirectoryTitle = (EditText)findViewById(R.id.etDirectoryTitle);
        etDirectoryTitle.setHint(R.string.guide_title);

        // 메모어댑터 설정
        memoAdapter = new MemoAdapter(this, R.layout.listview_memo, memoList, new MemoAdapter.LocationBtnClickListener() {
            @Override
            public void onLocationClick(int position, ToggleButton btnLo) {
 //               boolean isCheck = btnLo.isChecked();
                Intent i = new Intent(AddDirectoryActivity.this, LocationActivity.class);
                if(memoList.get(position).getLocationOption().getTitle()!=null) { // 위치 메모가 이미 설정 되어 있을 때
                    btnLo.setChecked(true);
                    LocationOption locationOption = memoList.get(position).getLocationOption();
                    i.putExtra("location_title",locationOption.getTitle());
                    i.putExtra("location_address",locationOption.getAddress());
                    i.putExtra("location_latitude", locationOption.getLatitude());
                    i.putExtra("location_longitude",locationOption.getLongitude());
                    i.putExtra("location_phone",locationOption.getPhone());
                    i.putExtra("location_destination",locationOption.getDestination());
                    i.putExtra("switch",memoList.get(position).isAlarmOff());
                    i.putExtra("isReset",true);
                }

                else {
                    i.putExtra("isReset",false);
                }

                i.putExtra("whatActivity", "add");
                i.putExtra("location_position", position);

                if(memoList.get(position).getCategoryOption().getName() !=null) {

                    final int pos = position;
                    final Intent newIn = i;

                    View.OnClickListener leftListener = new View.OnClickListener() {
                        public void onClick(View v) {

                            CategoryOption categoryOption = new CategoryOption();
                            memoList.get(pos).setMemoType(MemoType.Location);
                            memoList.get(pos).setCategoryOption(categoryOption);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });

                            startActivityForResult(newIn, REQ_LOCATION_CODE);

                            mCustomDialog.dismiss();
                        }
                    };

                    View.OnClickListener rightListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            mCustomDialog.dismiss();
                        }
                    };

                    mCustomDialog = new CustomDialog(AddDirectoryActivity.this,
                            "위치 메모로 변경하시겠습니까?\n기존 설정은 사라집니다.", // 내용
                            leftListener, // 왼쪽 버튼 이벤트
                            rightListener); // 오른쪽 버튼 이벤트
                    mCustomDialog.show();

                }

                else {
                    memoList.get(position).setMemoType(MemoType.Location);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });
                    startActivityForResult(i, REQ_LOCATION_CODE);
                }

            }
        }, new MemoAdapter.CategoryBtnClickListener() {
            @Override
            public void onCategoryClick(int position, ToggleButton btnCa) {
                boolean isCheck = btnCa.isChecked();
                Intent i = new Intent(AddDirectoryActivity.this, CategoryActivity.class);
                if (memoList.get(position).getCategoryOption().getName()!=null) {
                    btnCa.setChecked(true);
                    CategoryOption categoryOption = memoList.get(position).getCategoryOption();
                    i.putExtra("category_name", categoryOption.getName());
                    i.putExtra("category_detail", categoryOption.getDetails());
                    i.putExtra("category_radius", categoryOption.getRadius());
                    i.putExtra("switch",memoList.get(position).isAlarmOff());
                }

                else {

                }

                i.putExtra("whatActivity","add");
                i.putExtra("category_position", position);

                if (memoList.get(position).getLocationOption().getTitle()!=null) {
                    final int pos = position;
                    final Intent newIn = i;

                    View.OnClickListener leftListener = new View.OnClickListener() {
                        public void onClick(View v) {

                            LocationOption locationOption = new LocationOption();
                            memoList.get(pos).setMemoType(MemoType.Category);
                            memoList.get(pos).setLocationOption(locationOption);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });

                            startActivityForResult(newIn, REQ_CATEGORY_CODE);

                            mCustomDialog.dismiss();
                        }
                    };

                    View.OnClickListener rightListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            mCustomDialog.dismiss();
                        }
                    };

                    mCustomDialog = new CustomDialog(AddDirectoryActivity.this,
                            "카테고리 메모로 변경하시겠습니까?\n기존 설정은 사라집니다.", // 내용
                            leftListener, // 왼쪽 버튼 이벤트
                            rightListener); // 오른쪽 버튼 이벤트
                    mCustomDialog.show();

                }

                // 위치 메모가 설정 되어 있지 않을 때, 카테고리 메모 설정 시
                else {
                    memoList.get(position).setMemoType(MemoType.Category);
                    startActivityForResult(i, REQ_CATEGORY_CODE);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

        lvMemo = (ListView) findViewById(R.id.lvMemo);
        lvMemo.setAdapter(memoAdapter);
        lvMemo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        // 리스트뷰 아이템 롱 클릭 시 메모 다중 삭제 모드 진입
        lvMemo.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                if (!isDeleteMode) {
                    isDeleteMode = true;
                    btnSaveDirectory.setVisibility(View.GONE);
                    btnDeleteMemo.setVisibility(View.VISIBLE);
                    btnAddMemo.setVisibility(View.GONE);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });
                    return true;
                }
                else
                    return false;
            }
        });

        // 새로운 메모 추가
        btnAddMemo = (ImageButton) findViewById(R.id.btnAddMemo);
        btnAddMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (memoList.get(memoList.size() - 1).getText() == null) { // 작성 중인 메모 내용 비어있으면 추가 못함
                    Toast toast = Toast.makeText(AddDirectoryActivity.this, "작성 중인 메모가 있습니다.", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    if (memoList.get(memoList.size() - 1).getText().equals("")) {
                        Toast toast = Toast.makeText(AddDirectoryActivity.this, "작성 중인 메모가 있습니다.", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        AMemo memo = new AMemo();
                        memoList.add(memo);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                memoAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }
        });


        // 디렉토리 저장
        btnSaveDirectory = (Button)findViewById(R.id.btnSaveDirectory);
        btnSaveDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(memoList.size()==1 && (memoList.get(0).getText() == null || memoList.get(0).getText().equals(""))) {

                    Toast toast = Toast.makeText(AddDirectoryActivity.this,"저장된 메모가 없어 메모 작성을 취소합니다.",Toast.LENGTH_SHORT);
                    toast.show();
                    Intent i = new Intent(AddDirectoryActivity.this, MainActivity.class);
                    setResult(Activity.RESULT_CANCELED, i);
                    finish();
                }

                else {

                    RootDirectory tmpDir = new RootDirectory();

                    if (etDirectoryTitle.getText().toString().equals("") || etDirectoryTitle.getText() == null) {
                        tmpDir.setTitle(memoList.get(0).getText());
                    } else tmpDir.setTitle(etDirectoryTitle.getText().toString());

                    for (int i = 0; i < memoList.size(); i++) {

                        AMemo memo = memoList.get(i);
                        memo.setId(i); //hashMap이어서 임시로 id 지정

                        if (memoList.get(i).getText() != null && memoList.get(i).getText().length() != 0)
                            tmpDir.addMemo(memo);
                    }

                    dataManager.addDirectory(tmpDir);
                    Intent i = new Intent(AddDirectoryActivity.this, MainActivity.class);
                    Toast.makeText(AddDirectoryActivity.this, "메모가 저장되었습니다.", Toast.LENGTH_LONG).show();
                    i.putExtra("addDir", tmpDir.getId());
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            }
        });

        // 메모 삭제
        btnDeleteMemo = (ImageButton) findViewById(R.id.btnDeleteMemo);
        btnDeleteMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<Integer, Boolean> delMap = memoAdapter.getCheckedList();

                final List<AMemo> newMemoList = new ArrayList<AMemo>();

                for (int i = 0; i < memoList.size(); i++) {
                    if (!delMap.get(i)) {
                        newMemoList.add(memoList.get(i));
                    }
                }

                View.OnClickListener leftListener = new View.OnClickListener() {
                    public void onClick(View v) {

                        if (newMemoList.size() == 0) { // 메모 전부 다 삭제 할 때

                            AMemo memo = new AMemo();

                            memoList.clear();
                            memoList.add(memo);

                            isDeleteMode = false;
                            btnSaveDirectory.setVisibility(View.VISIBLE);
                            btnDeleteMemo.setVisibility(View.GONE);
                            btnAddMemo.setVisibility(View.VISIBLE);

                        }

                        else {

                            memoList.clear();
                            memoList.addAll(newMemoList);

                            isDeleteMode = false;
                            btnSaveDirectory.setVisibility(View.VISIBLE);
                            btnDeleteMemo.setVisibility(View.GONE);
                            btnAddMemo.setVisibility(View.VISIBLE);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    memoAdapter.notifyDataSetChanged();
                                }
                            });
                        }

                        mCustomDialog.dismiss();
                    }
                };

                View.OnClickListener rightListener = new View.OnClickListener() {
                    public void onClick(View v) {
                        mCustomDialog.dismiss();
                    }
                };

                mCustomDialog = new CustomDialog(AddDirectoryActivity.this,
                        "메모를 삭제하시겠습니까?", // 내용
                        leftListener, // 왼쪽 버튼 이벤트
                        rightListener); // 오른쪽 버튼 이벤트
                mCustomDialog.show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        //super.onBackPressed();

        if(!isDeleteMode) {

            if(memoList.size()==1 && (memoList.get(0).getText() == null || memoList.get(0).getText().equals(""))) {

                Toast toast = Toast.makeText(AddDirectoryActivity.this,"저장된 메모가 없어 메모 작성을 취소합니다.",Toast.LENGTH_SHORT);
                toast.show();
                Intent i = new Intent(AddDirectoryActivity.this, MainActivity.class);
                setResult(Activity.RESULT_CANCELED, i);
                finish();
            }

            else {

                RootDirectory tmpDir = new RootDirectory();

                if (etDirectoryTitle.getText().toString().equals("") || etDirectoryTitle.getText() == null) {
                    tmpDir.setTitle(memoList.get(0).getText());
                } else tmpDir.setTitle(etDirectoryTitle.getText().toString());

                for (int i = 0; i < memoList.size(); i++) {

                    AMemo memo = memoList.get(i);
                    memo.setId(i); //hashMap이어서 임시로 id 지정

                    if (memoList.get(i).getText() != null && memoList.get(i).getText().length() != 0)
                        tmpDir.addMemo(memo);
                }

                dataManager.addDirectory(tmpDir);
                Intent i = new Intent(AddDirectoryActivity.this, MainActivity.class);
                i.putExtra("addDir", tmpDir.getId());
                Toast.makeText(AddDirectoryActivity.this, "메모가 저장되었습니다.", Toast.LENGTH_LONG).show();
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        }

        else {
            isDeleteMode = false;
            btnSaveDirectory.setVisibility(View.VISIBLE);
            btnDeleteMemo.setVisibility(View.GONE);
            btnAddMemo.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_LOCATION_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:

                    LocationOption location = new LocationOption();
                    location.setTitle(data.getStringExtra("location_title"));
                    location.setAddress(data.getStringExtra("location_address"));
                    location.setLongitude(data.getDoubleExtra("location_longitude",0));
                    location.setLatitude(data.getDoubleExtra("location_latitude",0));
                    location.setPhone(data.getStringExtra("location_phone"));
                    location.setRadius(data.getIntExtra("location_radius",100));

                    if(!data.getBooleanExtra("isReset",false)) {
                        Destination des = new Destination();
                        ArrayList<Item> desList = new ArrayList<>();
                        desList.add((Item) data.getSerializableExtra("OBJECT"));
                        des.setDestinationList(desList);
                        location.setDestination(des);
                    }

                    else {
                        Destination des = (Destination) data.getSerializableExtra("location_destination");
                        location.setDestination(des);
                    }
                    memoList.get(data.getIntExtra("location_position",0)).setAlarmOff(data.getBooleanExtra("switch",false));
                    if(memoList.get(data.getIntExtra("location_position",0)).isAlarmOff()) {
                        memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Normal);
                    }
                    else {
                        memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Location);
                    }
                    memoList.get(data.getIntExtra("location_position",0)).setLocationOption(location);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });

                    break;

                case Activity.RESULT_CANCELED :

                    //LocationOption locationNullOption = new LocationOption();

                    if(memoList.get(data.getIntExtra("location_position",0)).getLocationOption().getTitle() == null)
                    {
                        memoList.get(data.getIntExtra("location_position",0)).setMemoType(MemoType.Normal);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                memoAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    //memoList.get(data.getIntExtra("category_position",0)).setLocationOption(locationNullOption);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            memoAdapter.notifyDataSetChanged();
//                        }
//                    });
            }
        }

        else if(requestCode == REQ_CATEGORY_CODE) {

            switch (resultCode) {
                case Activity.RESULT_OK:

                    CategoryOption categoryOption = new CategoryOption();
                    categoryOption.setName(data.getStringExtra("category_name"));
                    categoryOption.setDetails(data.getStringExtra("category_detail"));
//                    categoryOption.setRadius(data.getDoubleExtra("category_radius",0));
                    memoList.get(data.getIntExtra("category_position",0)).setAlarmOff(data.getBooleanExtra("switch",false));
                    if(memoList.get(data.getIntExtra("category_position",0)).isAlarmOff()) {
                        memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Normal);
                    }
                    else {
                        memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Category);
                    }

                    memoList.get(data.getIntExtra("category_position",0)).setCategoryOption(categoryOption);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            memoAdapter.notifyDataSetChanged();
                        }
                    });

                    break;

                case Activity.RESULT_CANCELED :

                    if(memoList.get(data.getIntExtra("category_position",0)).getCategoryOption().getName() == null)
                    {
                        memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Normal);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                memoAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    //CategoryOption categoryNullOption = new CategoryOption();

                    //memoList.get(data.getIntExtra("category_position",0)).setMemoType(MemoType.Normal);
                    //memoList.get(data.getIntExtra("category_position",0)).setCategoryOption(categoryNullOption);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            memoAdapter.notifyDataSetChanged();
//                        }
//                    });
            }
        }
    }
}

class MemoAdapter extends BaseAdapter implements View.OnClickListener{

    Context context;
    LayoutInflater inflater;
    int layout;
    List<AMemo> list = new ArrayList<>();
    HashMap<Integer, Boolean> checkedList = new HashMap<>();
    LocationBtnClickListener locationBtnClickListener;
    CategoryBtnClickListener categoryBtnClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLocation:
                if (this.locationBtnClickListener != null) {
                    this.locationBtnClickListener.onLocationClick((int) v.getTag(R.id.position), (ToggleButton) v.getTag(R.id.ischecked));
                }
                break;

            case R.id.btnCategory:
                if (this.categoryBtnClickListener != null) {
                    this.categoryBtnClickListener.onCategoryClick((int) v.getTag(R.id.position), (ToggleButton) v.getTag(R.id.ischecked));
                }
        }
    }

    public interface LocationBtnClickListener {
        void onLocationClick(int position, ToggleButton btnLo);
    }

    public interface CategoryBtnClickListener {
        void onCategoryClick(int position, ToggleButton btnCa);
    }

    public MemoAdapter(Context context, int layout, List<AMemo> memoList, LocationBtnClickListener locationClickListener, CategoryBtnClickListener categoryClickListener){
        super();
        this.context = context;
        this.layout = layout;
        this.locationBtnClickListener = locationClickListener;
        this.categoryBtnClickListener = categoryClickListener;
        list= memoList;
        checkedList = new HashMap<>();
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public HashMap<Integer,Boolean> getCheckedList() { return checkedList; }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

//        if (convertView == null) {
//            convertView = inflater.inflate(R.layout.listview_memo, parent, false);
//        }

        // ADD화면에서는 listView 재사용XXX
        convertView = inflater.inflate(R.layout.listview_memo, parent, false);

        //TOGGLE BTN에 따라서 TYPE 지정
        final ToggleButton btnCategory = (ToggleButton)convertView.findViewById(R.id.btnCategory);
        final ToggleButton btnLocation = (ToggleButton)convertView.findViewById(R.id.btnLocation);

        btnLocation.setOnClickListener(this);
        btnLocation.setTag(R.id.position,position);
        btnLocation.setTag(R.id.ischecked,btnLocation);

        btnCategory.setOnClickListener(this);
        btnCategory.setTag(R.id.position,position);
        btnCategory.setTag(R.id.ischecked,btnCategory);

        /*

        btnLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(btnCategory.isChecked()) {
                    Toast toast = Toast.makeText(context,"위치 메모로 변경, 확인창 띄울 예정",Toast.LENGTH_SHORT);
                    toast.show();
                    btnCategory.setChecked(false);
                }
                else {

                }
            }
        });

        btnCategory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(btnLocation.isChecked()) {
                    Toast toast = Toast.makeText(context,"카테고리 메모로 변경, 확인창 띄울 예정",Toast.LENGTH_SHORT);
                    toast.show();
                    btnLocation.setChecked(false);
                }
                else {

                }
            }
        });

        */

        final EditText etMemo = (EditText)convertView.findViewById(R.id.etMemo);
        etMemo.setHint(R.string.guide_memo);

        final TextView tvMemoDetail = (TextView) convertView.findViewById(R.id.tvMemoDetail);

        //if(list.get(position).getText()!=null) etMemo.setText(list.get(position).getText());
        etMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(list.get(position).getText()==null || list.get(position).getText().equals("")) {
                    etMemo.requestFocus();
                    InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.showSoftInput(etMemo, InputMethodManager.SHOW_FORCED);
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            boolean isStart = false;

            @Override
            public void afterTextChanged(Editable s) {

                        list.get(position).setText(etMemo.getText().toString());
                        btnCategory.setVisibility(View.VISIBLE);
                        btnLocation.setVisibility(View.VISIBLE);
                etMemo.setSelection(etMemo.getText().toString().length());
            }
        });


        etMemo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus) {
                    if(position < list.size()) {

                        if (list.get(position).getMemoType() == MemoType.Location) {
                            if (etMemo.getText().toString().equals(""))
                                etMemo.setText(tvMemoDetail.getText().toString());
                        } else if (list.get(position).getMemoType() == MemoType.Category) {
                            if (etMemo.getText().toString().equals(""))
                                etMemo.setText(tvMemoDetail.getText().toString());
                        } else {

                            if (list.size() != 1 && etMemo.getText().toString().equals("")) {
                                list.remove(position);
                                notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
        });


        if(list.get(position).getMemoType() == MemoType.Category) {
            etMemo.setText(list.get(position).getText());
            if(list.get(position).getCategoryOption().getDetails() != null) tvMemoDetail.setText(list.get(position).getCategoryOption().getName()+" | " +list.get(position).getCategoryOption().getDetails());
            tvMemoDetail.setVisibility(View.VISIBLE);
            btnCategory.setChecked(true);
            if(etMemo.getText().toString().equals("") || etMemo.getText() == null) etMemo.setText(tvMemoDetail.getText().toString());
        }

        if(list.get(position).getMemoType() == MemoType.Location) {
            etMemo.setText(list.get(position).getText());
            if(list.get(position).getLocationOption().getTitle() != null) tvMemoDetail.setText(list.get(position).getLocationOption().getTitle());
            tvMemoDetail.setVisibility(View.VISIBLE);
            btnLocation.setChecked(true);
            if(etMemo.getText().toString().equals("") ||  etMemo.getText() == null) etMemo.setText(tvMemoDetail.getText().toString());
        }

        if(list.get(position).getMemoType() == MemoType.Normal) {
            etMemo.setText(list.get(position).getText());
            btnCategory.setChecked(false);
            btnLocation.setChecked(false);
            tvMemoDetail.setText("");
            tvMemoDetail.setVisibility(View.GONE);
        }

        ImageView oval = (ImageView) convertView.findViewById(R.id.oval);

        CheckBox cbDelete = (CheckBox) convertView.findViewById(R.id.cbDelete);
        if(AddDirectoryActivity.isDeleteMode) {
            cbDelete.setVisibility(View.VISIBLE);
            cbDelete.bringToFront();
            cbDelete.setChecked(false);
            oval.setVisibility(View.GONE);
            btnCategory.setVisibility(View.GONE);
            btnLocation.setVisibility(View.GONE);
        }

        else {
            cbDelete.setVisibility(View.GONE);
            oval.setVisibility(View.VISIBLE);
            btnCategory.setVisibility(View.VISIBLE);
            btnLocation.setVisibility(View.VISIBLE);
        }

        if(cbDelete.isChecked()) {
            checkedList.put(position,true);
        }
        else {
            checkedList.put(position,false);
        }

        cbDelete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    checkedList.put(position,true);
                }

                else {
                    checkedList.put(position,false);
                }
            }
        });

        return convertView;
    }
}


