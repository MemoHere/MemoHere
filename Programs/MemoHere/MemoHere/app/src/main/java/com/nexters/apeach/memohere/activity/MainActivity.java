package com.nexters.apeach.memohere.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.MemoType;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.processor.service.BackService;
import com.sa90.materialarcmenu.ArcMenu;
import com.sa90.materialarcmenu.StateChangeListener;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    /*
     * Created by Soyeon on 2017-07-17
     */

    /*
     * Revised by Jiwon on 2017-07-19
     *  옵션메뉴로 메모 추가 -> 버튼 클릭으로 메모 추가
     *  롱클릭 시 바로 삭제 -> 팝업 추가
     */

    /*
     * Revised by Jiwon on 2017-08-02
     */

    private final int REQ_CODE = 100;
    final int LOCATION_PERMISSION_CODE = 0;
    final int CALL_PERMISSION_CODE = 1;

    DataManager dataManager;
    List<RootDirectory> directoryList;

    public Button btnAddDirectory;
    public EditText etSearchMemo;
    public ExpandableListView lvDirectory;
    public DirectoryExAdapter dirAdapter; // 시간을 함께 표시하기 위해서 커스텀 어댑터 사용

    private CustomDialog mCustomDialog;

    ArcMenu fabMenu;
    ImageButton btnHome;
    ImageButton btnAlarm;
    ImageButton btnGarbage;

    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tryPermCheck();

        btnAddDirectory = (Button) findViewById(R.id.btnAddDirectory);

        dataManager = DataManager.createInstance(this);
        directoryList = dataManager.getMainList();
        dirAdapter = new DirectoryExAdapter(this,R.layout.listview_directory,directoryList);
        lvDirectory = (ExpandableListView)findViewById(R.id.lvDirectory);
        lvDirectory.setAdapter(dirAdapter);

        btnAddDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddDirectoryActivity.class);
                startActivityForResult(intent, REQ_CODE);
            }
        });

        lvDirectory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        lvDirectory.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                int itemType = ExpandableListView.getPackedPositionType(id);
                final int groupPosition = ExpandableListView.getPackedPositionGroup(id);

                if(itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {

                    View.OnClickListener leftListener = new View.OnClickListener() {
                        public void onClick(View v) {

                            dataManager.moveTrashDirectory(directoryList.get(groupPosition));
                            directoryList.remove(groupPosition);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dirAdapter.notifyDataSetChanged();
                                }
                            });
                            mCustomDialog.dismiss();
                        }
                    };

                    View.OnClickListener rightListener = new View.OnClickListener() {
                        public void onClick(View v) {
                            mCustomDialog.dismiss();
                        }
                    };

                    mCustomDialog = new CustomDialog(MainActivity.this,
                            "삭제하시겠습니까?", // 내용
                            leftListener, // 왼쪽 버튼 이벤트
                            rightListener); // 오른쪽 버튼 이벤트
                    mCustomDialog.show();

                    return true;

                } else {
                    return false;
                }
            }
        });

        if(directoryList.size()>0)
            lvDirectory.expandGroup(directoryList.size()-1);

        lvDirectory.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                Intent intent = new Intent(MainActivity.this, UpdateDirectoryActivity.class);
                intent.putExtra("update_data", directoryList.get(groupPosition).getId());
                startActivityForResult(intent, REQ_CODE);
                return true;
            }
        });

        etSearchMemo = (EditText) findViewById(R.id.etSearchMemo);
        etSearchMemo.setHint(R.string.guide_search);
        etSearchMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String filterText = s.toString();
                ((DirectoryExAdapter) lvDirectory.getExpandableListAdapter()).getFilter().filter(filterText);
            }
        });

        //FAB MENU 를 위한 설정 (ARC MENU)
        fabMenu = (ArcMenu)findViewById(R.id.fabMenu);
        fabMenu.setStateChangeListener(new StateChangeListener() {
            @Override
            public void onMenuOpened() {
            }
            @Override
            public void onMenuClosed() {

            }
        });
        btnHome = (ImageButton)findViewById(R.id.btnHome);
        btnAlarm = (ImageButton)findViewById(R.id.btnAlarm);
        if(getAlarmOnOff()) {
            btnAlarm.setTag(R.drawable.btn_alarm_on);
            btnAlarm.setImageResource(R.drawable.btn_alarm_on);
        }
        else {
            btnAlarm.setTag(R.drawable.btn_alarm_off);
            btnAlarm.setImageResource(R.drawable.btn_alarm_off);
        }

        btnGarbage = (ImageButton)findViewById(R.id.btnGarbage);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabMenu.toggleMenu();
            }
        });
        btnAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(btnAlarm.getTag().equals(R.drawable.btn_alarm_on)){

                    btnAlarm.setImageResource(R.drawable.btn_alarm_off);
                    btnAlarm.setTag(R.drawable.btn_alarm_off);

                    Toast.makeText(MainActivity.this, "전체 알림이 꺼졌습니다", Toast.LENGTH_SHORT).show();
                } else{

                    btnAlarm.setImageResource(R.drawable.btn_alarm_on);
                    btnAlarm.setTag(R.drawable.btn_alarm_on);

                    Toast.makeText(MainActivity.this, "전체 알림이 켜졌습니다", Toast.LENGTH_SHORT).show();
                }

                setNotifyService();
                setAlarmOnOff();
            }
        });
        btnGarbage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, TrashActivity.class);
                startActivityForResult(intent, REQ_CODE);
            }
        });

        this.setNotifyService();

        //앱 첫 실행 시 가이드 페이지 띄우기
        SharedPreferences pref = getSharedPreferences("isFirst", Activity.MODE_PRIVATE);
        boolean first = pref.getBoolean("isFirst", false);
        if(first==false){
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("isFirst",true);
            editor.commit();

            viewPager= (ViewPager)findViewById(R.id.vp);
            CustomAdapter adapter= new CustomAdapter(getLayoutInflater());
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    if(position == 4) viewPager.setVisibility(View.GONE);
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }else{

        }


    }

    private void setNotifyService(){

        Intent i = new Intent(MainActivity.this, BackService.class);
        if(btnAlarm.getTag().equals(R.drawable.btn_alarm_on)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                this.startService(i);
            }
            else{

//                Toast.makeText(this, "GPS를 켠 후 다시 실행해주세요", Toast.LENGTH_SHORT).show();
            }
        }
        else {

            this.stopService(i);
        }
    }

    //휴지통에 있던 메모가  복원 되었을 때  listview에 연결된 adapter를 갱신한다
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    int delId = data.getIntExtra("delete_memo",-1);
                    if(delId != -1) dataManager.removeDirectory(dataManager.getDirectory(delId));
                    directoryList.clear();
                    directoryList.addAll(dataManager.getMainList());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dirAdapter.notifyDataSetChanged();
                            lvDirectory.setAdapter(dirAdapter);
                            dirAdapter.sort();
                        }
                    });
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        dataManager.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataManager.close();
    }



    public boolean getAlarmOnOff(){

        SharedPreferences sp = this.getSharedPreferences("Noti", this.MODE_PRIVATE);
        return sp.getBoolean("AlarmOnOff",true);
    }

    public void setAlarmOnOff(){

        SharedPreferences sp = this.getSharedPreferences("Noti", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putBoolean("AlarmOnOff", btnAlarm.getTag().equals(R.drawable.btn_alarm_on));
        editor.commit();
    }

    //PERMISSON
    public void tryPermCheck(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE},LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case LOCATION_PERMISSION_CODE :
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED ) {
                    
                    //start service
                    this.setNotifyService();
                }
                else{
                    //1
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                    alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
                    alert.setMessage("권한 설정이 필요합니다.");
                    alert.show();

                }
                break;


        }
    }

}

class DirectoryExAdapter extends BaseExpandableListAdapter implements Filterable {

    Context context;
    LayoutInflater inflater;
    int layout;
    List<RootDirectory> list;
    List<RootDirectory> filteredList;
    Filter listFilter;


    public DirectoryExAdapter(Context context, int layout, List<RootDirectory> dirList){
        super();
        this.context = context;
        this.layout = layout;
        list= dirList;
        filteredList = list;

        sort();

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void sort(){

        //HashMap 시간 순(CreationTime) 정렬
        Collections.sort(list, new Comparator<RootDirectory>() {
            @Override
            public int compare(RootDirectory o1, RootDirectory o2) {
                return o1.getCreateTime().compareTo(o2.getCreateTime());
            }
        });
        Collections.reverse(list);

    }

    private class ListFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults() ;

            if (constraint == null || constraint.length() == 0) {
                results.values = list ;
                results.count = list.size() ;

            } else {

                ArrayList<RootDirectory> itemList = new ArrayList<>() ;
                for (RootDirectory item : list) {
                    boolean isContain = false;
                    for(AMemo memo : item.getMemoList()) {
                        if(memo.getText().toUpperCase().contains(constraint.toString().toUpperCase())){
                            if(!isContain) {
                                itemList.add(item);
                                isContain = true;
                            }
                        }
                    }

                    if (item.getTitle().toUpperCase().contains(constraint.toString().toUpperCase()))
                    {
                        if(!isContain) {
                            itemList.add(item);
                        }
                    }
                }

                results.values = itemList ;
                results.count = itemList.size() ;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredList = (ArrayList<RootDirectory>) results.values ;

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

    @Override public Filter getFilter()
    {
        if (listFilter == null) {
            listFilter = new ListFilter() ;
        } return listFilter;
    }

    @Override
    public int getGroupCount() {
        return filteredList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return filteredList.get(groupPosition).getMemoList().size();
    }

    @Override
    public RootDirectory getGroup(int groupPosition) {
        return null;
    }

    @Override
    public AMemo getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.listview_directory, parent, false);
        }

        TextView textTV = (TextView)convertView.findViewById(R.id.textTV);
//        TextView dateTV = (TextView)convertView.findViewById(R.id.dateTV);

        textTV.setText(filteredList.get(groupPosition).getTitle());
//        dateTV.setText(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(list.get(groupPosition).getCreateTime()));

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.memo_row, parent, false);
        }

        TextView memoRowTV = (TextView) convertView.findViewById(R.id.memoRowTV);
        memoRowTV.setText(filteredList.get(groupPosition).getMemoList().get(childPosition).getText());

        TextView memoRowDetailTV = (TextView) convertView.findViewById(R.id.memoRowDetailTV);
        if(filteredList.get(groupPosition).getMemoList().get(childPosition).getMemoType() == MemoType.Category) {
            memoRowDetailTV.setText(filteredList.get(groupPosition).getMemoList().get(childPosition).getCategoryOption().getName()
            +" | " + filteredList.get(groupPosition).getMemoList().get(childPosition).getCategoryOption().getDetails());
            memoRowDetailTV.setVisibility(View.VISIBLE);
        }

        else if(filteredList.get(groupPosition).getMemoList().get(childPosition).getMemoType() == MemoType.Location) {
            memoRowDetailTV.setText(filteredList.get(groupPosition).getMemoList().get(childPosition).getLocationOption().getTitle());
            memoRowDetailTV.setVisibility(View.VISIBLE);
        }

        else if(filteredList.get(groupPosition).getMemoList().get(childPosition).getMemoType() == MemoType.Normal) {
            memoRowDetailTV.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

//가이드 페이지에 사용하는 adapter
class CustomAdapter extends PagerAdapter {

    LayoutInflater inflater;

    public CustomAdapter(LayoutInflater  inflater) { this.inflater=inflater;  }

    @Override
    public int getCount() { return 5;}

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view=null;
        view= inflater.inflate(R.layout.viewpager_childview, null);

        ImageView img= (ImageView)view.findViewById(R.id.img_viewpager_childimage);

        if(position == 4) return null;

        img.setImageResource(R.drawable.guide_01+position);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) { container.removeView((View)object); }

    @Override
    public boolean isViewFromObject(View v, Object obj) {   return v==obj;  }
}
