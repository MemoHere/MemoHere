package com.nexters.apeach.memohere.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.processor.service.BackService;
import com.sa90.materialarcmenu.ArcMenu;

import java.util.ArrayList;
import java.util.List;

public class TrashActivity extends AppCompatActivity {

    private final int REQ_CODE = 100;

    DataManager dataManager;

    List<RootDirectory> directoryList;
    public ListView lvDirectory;
    public TrashAdapter dirAdapter;
    public ArrayAdapter<String> dialogAdapter;

    public Button btnTrashDelete;
    public EditText etSearchMemo;

    ArcMenu fabMenu;
    ImageButton btnHome;
    ImageButton btnAlarm;
    ImageButton btnGarbage;

    private CustomDialog mCustomDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash);

        dataManager = DataManager.createInstance(this);
        directoryList = dataManager.getTrashList();
        dirAdapter = new TrashAdapter(this,R.layout.listview_trash,directoryList);
        lvDirectory = (ListView)findViewById(R.id.lvDirectory);

        //삭제를 위한 Dialog 리스트 생성
        dialogAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        dialogAdapter.addAll("복원","삭제","취소");
        dialogAdapter.notifyDataSetChanged();

        lvDirectory.setAdapter(dirAdapter);
        lvDirectory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detail = new Intent(TrashActivity.this, TrashDetailActivity.class);
                detail.putExtra("dirId", directoryList.get(position).getId());
                startActivityForResult(detail, REQ_CODE);
            }
        });

        btnTrashDelete = (Button)findViewById(R.id.btnTrashDelete);
        btnTrashDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCustomDialog = new CustomDialog(TrashActivity.this,
                        "모두 삭제하시겠습니까?", // 내용
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dataManager.removeDirectoryALL();
                                directoryList.clear();
                                dirAdapter.notifyDataSetChanged();
                                mCustomDialog.dismiss();
                            }
                        }, // 왼쪽 버튼 이벤트
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mCustomDialog.dismiss();
                            }
                        }
                ); // 오른쪽 버튼 이벤트
                        mCustomDialog.show();

//                AlertDialog.Builder deleteDialogBuilder = new AlertDialog.Builder(TrashActivity.this);
//                deleteDialogBuilder.setTitle("MemoHere")
//                        .setIcon(R.drawable.memohere_icon)
//                        .setMessage("메모를 모두 영구적으로 삭제하시겠습니까?")
//                        .setPositiveButton("확인",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(
//                                            DialogInterface dialog, int id) {
//                                        dataManager.removeDirectoryALL();
//                                        directoryList.clear();
//                                        dirAdapter.notifyDataSetChanged();
//                                    }
//                                })
//                        .setNegativeButton("취소",
//                                new DialogInterface.OnClickListener() {
//                                    public void onClick(
//                                            DialogInterface dialog, int id) {dialog.cancel();}
//                                });
//
//                AlertDialog deleteDialog = deleteDialogBuilder.create();
//                deleteDialog.show();
            }
        });
        //FAB MENU 를 위한 설정 (ARC MENU)
        fabMenu = (ArcMenu)findViewById(R.id.fabMenu);
        btnHome = (ImageButton)findViewById(R.id.btnHome);
        btnAlarm = (ImageButton)findViewById(R.id.btnAlarm);
        if(getAlarmOnOff()) {
            btnAlarm.setTag(R.drawable.btn_alarm_on);
            btnAlarm.setImageResource(R.drawable.btn_alarm_on);
        }
        else {
            btnAlarm.setTag(R.drawable.btn_alarm_off);
            btnAlarm.setImageResource(R.drawable.btn_alarm_off);
        }
        btnGarbage = (ImageButton)findViewById(R.id.btnGarbage);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TrashActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        btnAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //알람 off 시 이미지 변경
                if(btnAlarm.getTag().equals(R.drawable.btn_alarm_on)){
                    btnAlarm.setImageResource(R.drawable.btn_alarm_off);
                    btnAlarm.setTag(R.drawable.btn_alarm_off);

                    Toast.makeText(TrashActivity.this, "전체 알림이 꺼졌습니다", Toast.LENGTH_SHORT).show();

                } else{
                    btnAlarm.setImageResource(R.drawable.btn_alarm_on);
                    btnAlarm.setTag(R.drawable.btn_alarm_on);

                    Toast.makeText(TrashActivity.this, "전체 알림이 켜졌습니다", Toast.LENGTH_SHORT).show();
                }

                setNotifyService();
                setAlarmOnOff();
            }
        });
        btnGarbage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabMenu.toggleMenu();
            }
        });

        etSearchMemo = (EditText) findViewById(R.id.etSearchMemo);
        etSearchMemo.setHint(R.string.guide_search);
        etSearchMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String filterText = s.toString();
                ((TrashAdapter) lvDirectory.getAdapter()).getFilter().filter(filterText);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Intent MainIntent = new Intent(TrashActivity.this, MainActivity.class);
                    if("restore".equals(data.getSerializableExtra("type"))){
                    int id = (Integer) data.getSerializableExtra("dirId");
                    directoryList.remove(directoryList.indexOf(dataManager.getDirectory(id)));
                    }
                    else{
                        directoryList.clear();
                        directoryList.addAll(dataManager.getTrashList());
                    }
                    dirAdapter.notifyDataSetChanged();
                    setResult(Activity.RESULT_OK, MainIntent);
                    break;
            }
        }
    }

    public boolean getAlarmOnOff(){

        SharedPreferences sp = this.getSharedPreferences("Noti", this.MODE_PRIVATE);
        return sp.getBoolean("AlarmOnOff",true);
    }

    public void setAlarmOnOff(){

        SharedPreferences sp = this.getSharedPreferences("Noti", this.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putBoolean("AlarmOnOff", btnAlarm.getTag().equals(R.drawable.btn_alarm_on));
        editor.commit();
    }


    private void setNotifyService(){

        Intent i = new Intent(TrashActivity.this, BackService.class);
        if(btnAlarm.getTag().equals(R.drawable.btn_alarm_on)) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                this.startService(i);
            }
        }
        else {
            this.stopService(i);
        }
    }
}
class TrashAdapter extends BaseAdapter implements Filterable{

    Context context;
    LayoutInflater inflater;
    int layout;
    List<RootDirectory> list = new ArrayList<RootDirectory>();

    List<RootDirectory> filteredList;
    Filter listFilter;

    public TrashAdapter(Context context, int layout, List<RootDirectory> dirList){
        super();
        this.context = context;
        this.layout = layout;
        list= dirList;
        filteredList = list;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    class ListFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults() ;

            if (constraint == null || constraint.length() == 0) {
                results.values = list ;
                results.count = list.size() ;

            } else {

                ArrayList<RootDirectory> itemList = new ArrayList<>() ;
                for (RootDirectory item : list) {
                    boolean isContain = false;
                    for(AMemo memo : item.getMemoList()) {
                        if(memo.getText().toUpperCase().contains(constraint.toString().toUpperCase())){
                            if(!isContain) {
                                itemList.add(item);
                                isContain = true;
                            }
                        }
                    }
                    if (item.getTitle().toUpperCase().contains(constraint.toString().toUpperCase()))
                    {
                        if(!isContain) {
                            itemList.add(item);
                        }
                    }
                }
                results.values = itemList ;
                results.count = itemList.size() ;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredList = (ArrayList<RootDirectory>) results.values ;

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

    @Override
    public int getCount() { return filteredList.size();  }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }
        TextView textTV = (TextView)convertView.findViewById(R.id.tvTrashLine);
        textTV.setText(filteredList.get(position).getTitle());

       return convertView;
    }

    @Override
    public Filter getFilter() {
        if (listFilter == null) {
            listFilter = new ListFilter() ;
        } return listFilter;
    }
}
