package com.nexters.apeach.memohere.processor.service;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.content.Context;

import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.processor.MemoNotiManager;

/**
 * Created by shin on 2017. 7. 30..
 */

public class BackService extends Service {

    private ServiceThread serviceThread;
    private MemoNotiManager notiManager;


    public static boolean isServiceRunningCheck(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.example.apeach.memohere.processor.service.BackService".equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        registerRestartAlarm();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //set notification manager
        if(notiManager == null)
            notiManager = MemoNotiManager.createInstance(BackService.this, getSystemService(Context.NOTIFICATION_SERVICE));

        //start main background processor
        this.startThread();

        return START_REDELIVER_INTENT;
//        return super.onStartCommand(intent, flags, startId);
    }


    public void startThread(){

        if(this.serviceThread == null) {
            ServiceHandler handler = new ServiceHandler();
            serviceThread = new ServiceThread(this, handler);

            Thread thread = new Thread(serviceThread);
            thread.start();
        }
    }

    public void stopThread(){

        if(this.serviceThread != null) {
            this.serviceThread.stopProc();
            this.serviceThread = null;
        }
    }

    @Override
    public void onDestroy() {

        this.stopThread();
        notiManager = null;

        unregisterRestartAlarm();

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * 서비스가 시스템에 의해서 또는 강제적으로 종료되었을 때 호출되어
     * 알람을 등록해서 10초 후에 서비스가 실행되도록 한다.
     */
    private void registerRestartAlarm() {

        Intent intent = new Intent(BackService.this, AutoServiceReceiver.class);
        intent.setAction("ACTION.RESTART.BackService");
        PendingIntent sender = PendingIntent.getBroadcast(BackService.this, 0, intent, 0);

        long firstTime = SystemClock.elapsedRealtime();
        firstTime += 10 * 1000; // 10초 후에 알람이벤트 발생

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, firstTime, 10 * 1000, sender);
    }


    //등록되어있는 알람을 해제한다.
    private void unregisterRestartAlarm() {

        Intent intent = new Intent(BackService.this, AutoServiceReceiver.class);
        intent.setAction("ACTION.RESTART.BackService");
        PendingIntent sender = PendingIntent.getBroadcast(BackService.this, 0, intent, 0);

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.cancel(sender);
    }

    class ServiceHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

            AMemo memo = (AMemo)msg.obj;
            notiManager.notifyCustom(BackService.this, memo);
        }
    };
}
