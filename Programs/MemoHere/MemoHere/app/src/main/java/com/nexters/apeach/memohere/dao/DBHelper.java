package com.nexters.apeach.memohere.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Created by Soyeon on 2017-07-15.
 */

/*
*  Revised by Jiwon on 2017-07-17.
*  DB 명, TABLE 명 변경
*/




public class DBHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "MemoDB";
    public static final String DIRECTORY_TABLE_NAME = "DirectoryTable";
    public static final String MEMO_TABLE_NAME = "MemoTable";

    public DBHelper(Context context) {  super(context, DB_NAME, null, 1);}
    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTable = "create table " + DIRECTORY_TABLE_NAME + " ( _ID integer primary key autoincrement, TITLE text, CREATIONTIME datetime, UPDATETIME datetime, DELETED integer default 0 );";
        db.execSQL(createTable);

        createTable = "create table " + MEMO_TABLE_NAME + " ( _ID integer primary key autoincrement, ROOTID integer, MEMOTYPE text ,TEXT text, CREATIONTIME datetime, UPDATETIME datetime, ALARMOFF integer default 0," +
                /*CategoryMemo*/                  " CATEGORYNAME text, CATEGORYDETAILED text, CATEGORYRADIUS double" +
                /*LocationMemo*/                  ",TITLE text, ADDRESS text, LON double, LAT double, PHONE text, LOCATIONRADIUS double  );";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
