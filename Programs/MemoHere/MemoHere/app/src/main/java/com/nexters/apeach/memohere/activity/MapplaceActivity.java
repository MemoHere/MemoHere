package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.mapapi.MapApiConst;

import net.daum.mf.map.api.MapCircle;
import net.daum.mf.map.api.MapView;

import com.nexters.apeach.memohere.search.Item;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;

public class MapplaceActivity extends FragmentActivity implements MapView.MapViewEventListener {

    public Button searchloca;
    public TextView tvtitle, tvaddress, tvphone;
    private static final String LOG_TAG = "MapplaceActivity";
    public MapView mMapView;
    public EditText keyword;
    public double latitude, longitude;
    public String title, address, phone;
    public int itemId;
    public ImageView ivSelect;
    public Item item;

    public boolean switchflag = false;
    public Switch switchbtn;


    private int radius = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapplace);

        mMapView = (MapView)findViewById(R.id.map_view);
        mMapView.setDaumMapApiKey(MapApiConst.DAUM_MAPS_ANDROID_APP_API_KEY);
        mMapView.setMapViewEventListener(this);
        searchloca=(Button)findViewById(R.id.btnSearchLocaim);
        keyword=(EditText)findViewById(R.id.etSearchLocaim);
        ivSelect=(ImageView)findViewById(R.id.ivSelect);

        keyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MapplaceActivity.this,LocationActivity.class);
                intent.putExtra("Keyword",keyword.getText().toString());
                setResult(Activity.RESULT_CANCELED,intent);
                finish();
            }
        });

        ivSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //여기다 할거 하세여 피리님님

                Intent intent = new Intent(MapplaceActivity.this,LocationActivity.class);

                intent.putExtra("location_title", title);
                intent.putExtra("location_address", address);
                intent.putExtra("location_latitude", latitude);
                intent.putExtra("location_longitude", longitude);
                intent.putExtra("location_phone", phone);
                intent.putExtra("location_radius", radius);
                intent.putExtra("OBJECT",item);
                intent.putExtra("isReset",getIntent().getBooleanExtra("isReset",false));
                intent.putExtra("location_destination",getIntent().getSerializableExtra("location_destination"));
                intent.putExtra("location_position", getIntent().getIntExtra("location_position",0));
                intent.putExtra("whatActivity",getIntent().getStringExtra("whatActivity"));
                intent.putExtra("switch",switchflag);

                setResult(Activity.RESULT_OK,intent);
                finish();

           }

        });

        switchbtn = (Switch)findViewById(R.id.switch1);
        switchflag = getIntent().getBooleanExtra("switch",false);
        switchbtn.setChecked(!getIntent().getBooleanExtra("switch",false));

        switchbtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    switchflag = false;}
                else{
                    switchflag = true;
                }
            }
        });
    }

    public void onMapViewInitialized(MapView mapView) {
//        Log.i(LOG_TAG, "MapView had loaded. Now, MapView APIs could be called safely");

        Intent intent = getIntent();

        item = (Item) intent.getSerializableExtra("OBJECT");

        if(intent!=null){

            if(getIntent().getBooleanExtra("isReset",false)) {
                latitude = intent.getDoubleExtra("location_latitude", 37.537229);
                longitude = intent.getDoubleExtra("location_longitude", 127.005515);
                title = intent.getStringExtra("location_title");
                address = intent.getStringExtra("location_address");
                phone = intent.getStringExtra("location_phone");
            }

            else {

                title = item.title;
                longitude = item.longitude;
                latitude = item.latitude;
                address = item.address;
                phone = item.phone;
            }

            itemId = intent.getIntExtra("location_position",0);

            MapPoint mapPoint = MapPoint.mapPointWithGeoCoord(latitude,longitude);
            tvtitle = (TextView)findViewById(R.id.tvMapTitle);
            tvaddress = (TextView)findViewById(R.id.tvMapAddress);
            tvphone = (TextView)findViewById(R.id.tvMapPhone);

            tvtitle.setText(title);
            tvaddress.setText(address);
            tvphone.setText(phone);

            mMapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOff);
            mMapView.setMapCenterPointAndZoomLevel(mapPoint, 3, true);
            MapPOIItem marker =new MapPOIItem();
            marker.setItemName("result");
            marker.setMapPoint(mapPoint);
            marker.setMarkerType(MapPOIItem.MarkerType.CustomImage);
            marker.setCustomImageResourceId(R.drawable.kakao_talk_photo_2017_08_24_21_40_35);
            marker.setCustomImageAutoscale(false);
 //           marker.setCustomImageAnchor(0.5f,1.0f);
 //           marker.setShowCalloutBalloonOnTouch(false);
            mMapView.removeAllPOIItems();
            mMapView.addPOIItem(marker);

            MapCircle mapCircle = new MapCircle(
                    mapPoint,
                    radius,
                    Color.parseColor("#69ddcf"),
                    Color.parseColor("#4c69ddcf")
            );
            mMapView.removeAllCircles();
            mMapView.addCircle(mapCircle);

            tvaddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String adurl = "daummaps://look?p="+latitude+","+longitude;
                    Intent adintent = new Intent(Intent.ACTION_VIEW, Uri.parse(adurl));
                    startActivity(adintent);
                }
            });
            tvphone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String str = tvphone.getText().toString();
                    str="tel:"+str.replace("-","");
                    Intent phintent = new Intent("android.intent.action.DIAL",Uri.parse(str));
                    startActivity(phintent);
                }
            });
        }
    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapCenterPoint) {
    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int zoomLevel) {
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MapplaceActivity.this,LocationActivity.class);
        intent.putExtra("location_position",itemId);
        setResult(Activity.RESULT_CANCELED,intent);
        finish();
        super.onBackPressed();
    }
}
