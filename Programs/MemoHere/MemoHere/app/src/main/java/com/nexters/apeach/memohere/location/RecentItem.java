package com.nexters.apeach.memohere.location;

/**
 * Created by Hanbin Ju on 2017-08-09.
 */

public class RecentItem {
    private String recentkeyword;

    public void setRecent(String keyword){
        recentkeyword = keyword;
    }

    public String getRecent(){
        return this.recentkeyword;
    }
}
