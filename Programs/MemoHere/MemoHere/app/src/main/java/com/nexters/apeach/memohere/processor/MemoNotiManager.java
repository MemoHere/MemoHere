package com.nexters.apeach.memohere.processor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.service.notification.StatusBarNotification;
import android.support.v7.app.NotificationCompat;
import android.text.format.DateUtils;

import android.widget.RemoteViews;


import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.activity.NotificationDeatilsActivity;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.util.DebugToast;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shin on 2017. 7. 30..
 */

public class MemoNotiManager{

    public static final String EXTRA_DIR_KEY = "dirId";
    public static final String EXTRA_MEMO_KEY = "memoId";

    private boolean use;
    private long lastestDateForInitialize;
    private List<Integer> notTodayNotifyList;
    private NotificationManager notiManager;

    private Context serviceContext;

    private static MemoNotiManager instance;
    public static MemoNotiManager createInstance(Context context, Object notificationManager){

        if(instance == null){
            instance = new MemoNotiManager(context, notificationManager);
        }
        return  instance;
    }

    public static MemoNotiManager getInstance(){
        return instance;
    }

    public List<Integer> getNotTodayNotifyList(){

        SharedPreferences sp = serviceContext.getSharedPreferences("Noti", serviceContext.MODE_PRIVATE);
        String str = sp.getString("notTodayNotifyList", null);

        List<Integer> list= new ArrayList<>();
        if(str != null && str != "") {
            String[] Arr = str.split(";");

            for (String s : Arr) {
                list.add(Integer.parseInt(s));
            }
        }
        return list;
    }

    public long getLastestDateForInitialize(){

        SharedPreferences sp = serviceContext.getSharedPreferences("Noti", serviceContext.MODE_PRIVATE);

        return sp.getLong("lastestDateForInitialize",0);
    }

    public void setNotTodayNotifyInfo(){

        String str = "";

        SharedPreferences sp = serviceContext.getSharedPreferences("Noti", serviceContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putLong("lastestDateForInitialize", lastestDateForInitialize);
        editor.commit();

        if(notTodayNotifyList != null){
            for(int n : notTodayNotifyList){
                str += n + ";";
            }
            editor.putString("notTodayNotifyList", str);
            editor.commit();
        }
    }

    public void initNotTodayNotifyInfo(){

        this.notTodayNotifyList.clear();
        this.lastestDateForInitialize = System.currentTimeMillis();

        this.setNotTodayNotifyInfo();
    }

    private MemoNotiManager(Context context, Object notificationManager){

        if(this.notiManager == null) {
            this.notiManager = (NotificationManager) notificationManager;
            this.serviceContext = context;

            this.lastestDateForInitialize = this.getLastestDateForInitialize();
            this.notTodayNotifyList = this.getNotTodayNotifyList();
        }
    }

    public void notifyCustom(Context context, AMemo memo){

        //중지된 알람 리스트들이 하루가 지난 경우 초기화
        if(!DateUtils.isToday(this.lastestDateForInitialize)){

            this.initNotTodayNotifyInfo();
//            DebugToast.showOnThread(context, "언알람 리스트를 초기화합니다");
        }

        //알람이 꺼진 메모에 대해
        if(this.notTodayNotifyList.contains(memo.getId())) {

//            DebugToast.showOnThread(context, "알림이 꺼져있습니다 : " + memo.getText());
            return;
        }

        String title = "근처에 [" + memo.getUserTitle(10) + "] 이 있습니다";
        String content = "\"" + memo.getText() + "\"";

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.remoteview_notification);
        remoteViews.setTextViewText(R.id.notititle, title);
        remoteViews.setTextViewText(R.id.noticontent, content);
        remoteViews.setOnClickPendingIntent(R.id.notibutton, createCancelIntent(context, memo));


        NotificationCompat.Builder builder = new NotificationCompat.Builder(context.getApplicationContext());
         builder.setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(createClickIntent(context, memo))
                .setDeleteIntent(createDismissIntent(context, memo))
                .setContent(remoteViews)
                 .setAutoCancel(true)
                 .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);





//        Notification notification = new Notification.Builder(context.getApplicationContext())
////                .setTicker("Memo H.E.R.E")
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setPriority(Notification.PRIORITY_MAX)
//                .setContentIntent(createClickIntent(context, memo))
//                .setDeleteIntent(createDismissIntent(context, memo))
//                .setCustomContentView(remoteViews)
//                .build();

//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//        notification.flags |= Notification.FLAG_ONLY_ALERT_ONCE;
//        notification.defaults |= Notification.DEFAULT_SOUND;
//        notification.defaults |= Notification.DEFAULT_VIBRATE;

        notiManager.notify(memo.getId(), builder.build());
    }

    public void cancelNotification(int id){

        notiManager.cancel(id);
    }

    public Notification getNotification(int id){

        for (StatusBarNotification noti :notiManager.getActiveNotifications()) {
            if(id == noti.getId())
                return noti.getNotification();
        }

        return null;
    }

    public void addNoAlarmList(int memoId){

        if(!this.notTodayNotifyList.contains(memoId)){
            this.notTodayNotifyList.add(memoId);
        }

        this.setNotTodayNotifyInfo();
    }





    private PendingIntent createClickIntent(Context context, AMemo memo){

        //intent for click event handle
        Intent intent = new Intent(context, NotificationDeatilsActivity.class);
        intent.putExtra(EXTRA_DIR_KEY, memo.getParent().getId());
        intent.putExtra(EXTRA_MEMO_KEY, memo.getId());
        intent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }

    private PendingIntent createDismissIntent(Context context, AMemo memo){

        //intent for dismiss event handle
        Intent intent = new Intent(context, DismissNotificationReceiver.class);
        intent.putExtra(EXTRA_DIR_KEY, memo.getParent().getId());
        intent.putExtra(EXTRA_MEMO_KEY, memo.getId());
        intent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, intent, 0);

        return pendingIntent;
    }

    private PendingIntent createCancelIntent(Context context, AMemo memo){

        //intent for cancel event handle
        Intent intent = new Intent(context, CancelNotificationReceiver.class);
        intent.putExtra(EXTRA_DIR_KEY, memo.getParent().getId());
        intent.putExtra(EXTRA_MEMO_KEY, memo.getId());
        intent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }
}
