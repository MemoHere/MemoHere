package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.dto.memo.AMemo;

import java.util.ArrayList;
import java.util.List;

public class TrashDetailActivity extends AppCompatActivity {

    List<AMemo> memoList;
    public ListView lvMemo;
    public TrashDetailAdapter memoAdapter;
    DataManager dataManager;

    public Button btnTrashReturn;
    public Button btnTrashDelete;
    public EditText etSearchMemo;

    int pos;

    private CustomDialog mCustomDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash_detail);

        Intent intent = getIntent();
        pos = (int) intent.getSerializableExtra("dirId");

        dataManager = DataManager.createInstance(this);
        memoList = dataManager.getDirectory(pos).getMemoList();

        memoAdapter = new TrashDetailAdapter(this,R.layout.listview_trashdetail,memoList);

        lvMemo = (ListView)findViewById(R.id.lvDirectory);
        lvMemo.setAdapter(memoAdapter);

        btnTrashReturn = (Button)findViewById(R.id.btnTrashRestore);
        btnTrashReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCustomDialog = new CustomDialog(TrashDetailActivity.this,
                        "복구하시겠습니까?", // 내용
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dataManager.retunrTrashDIrectory(dataManager.getDirectory(pos));
                                Intent restore = new Intent(TrashDetailActivity.this, MainActivity.class);
                                restore.putExtra("type","restore");
                                restore.putExtra("dirId",pos);
                                setResult(Activity.RESULT_OK, restore);
                                finish();
                                mCustomDialog.dismiss();
                            }
                        }, // 왼쪽 버튼 이벤트
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mCustomDialog.dismiss();
                            }
                        }
                ); // 오른쪽 버튼 이벤트
                mCustomDialog.show();
            }
        });

        btnTrashDelete = (Button)findViewById(R.id.btnTrashDelete);
        btnTrashDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCustomDialog = new CustomDialog(TrashDetailActivity.this,
                        "삭제하시겠습니까?", // 내용
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dataManager.removeDirectory(dataManager.getDirectory(pos));
                                Intent delete = new Intent(TrashDetailActivity.this, MainActivity.class);
                                delete.putExtra("type","delete");
                                setResult(Activity.RESULT_OK, delete);
                                finish();
                                mCustomDialog.dismiss();
                            }
                        }, // 왼쪽 버튼 이벤트
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mCustomDialog.dismiss();
                            }
                        }
                ); // 오른쪽 버튼 이벤트
                mCustomDialog.show();
            }
        });

        etSearchMemo = (EditText) findViewById(R.id.etSearchMemo);
        etSearchMemo.setHint(R.string.guide_search);
        etSearchMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String filterText = s.toString();
                ((TrashDetailAdapter) lvMemo.getAdapter()).getFilter().filter(filterText);
            }
        });
    }

}
class TrashDetailAdapter extends BaseAdapter implements Filterable{

    Context context;
    LayoutInflater inflater;
    int layout;
    List<AMemo> list = new ArrayList<AMemo>();

    List<AMemo> filteredList;
    Filter listFilter;

    public TrashDetailAdapter(Context context, int layout, List<AMemo> memoList){
        super();
        this.context = context;
        this.layout = layout;
        list= memoList;
        filteredList=list;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Filter getFilter() {
        if (listFilter == null) {
            listFilter = new ListFilter() ;
        } return listFilter;
    }

    class ListFilter extends Filter{
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults() ;

            if (constraint == null || constraint.length() == 0) {
                results.values = list ;
                results.count = list.size() ;

            } else {
                ArrayList<AMemo> itemList = new ArrayList<>() ;

                for(AMemo m : list){
                    if(m.getText().toUpperCase().contains(constraint.toString().toUpperCase()))
                    itemList.add(m);
                }
                results.values = itemList ;
                results.count = itemList.size() ;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredList = (ArrayList<AMemo>) results.values ;

            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }
        TextView textTV = (TextView)convertView.findViewById(R.id.tvTrashDetail);
        textTV.setText("·   "+filteredList.get(position).getText());

        return convertView;
    }
}