package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;

import java.util.ArrayList;
import java.util.List;

public class DirectoryActivity extends AppCompatActivity {

    private final int REQ_CODE = 100;

    public TextView etDirectoryTitle;
    public Button btnUpdateDirectory;
    public ListView lvMemo;
    public DirDetailAdapter memoAdapter;

    DataManager dataManager;
    RootDirectory thisDirectory;
    List<AMemo> memoList;

    int pos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);

        Intent intent = getIntent();
        pos = (int) intent.getSerializableExtra("update_data");

        dataManager = DataManager.createInstance(this);
        thisDirectory = dataManager.getDirectory(pos);
        memoList = thisDirectory.getMemoList();

        etDirectoryTitle = (TextView) findViewById(R.id.tvDirectoryTitle);
        etDirectoryTitle.setTextColor(Color.BLACK);
        etDirectoryTitle.setText(thisDirectory.getTitle());

        memoAdapter = new DirDetailAdapter(this,R.layout.listview_dirdetail,memoList);
        lvMemo = (ListView) findViewById(R.id.lvMemo);
        lvMemo.setAdapter(memoAdapter);

        btnUpdateDirectory = (Button)findViewById(R.id.btnDirUpdate);
        btnUpdateDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DirectoryActivity.this, UpdateDirectoryActivity.class);
                intent.putExtra("update_data", pos);
                startActivityForResult(intent, REQ_CODE);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Intent intent = new Intent(DirectoryActivity.this,MainActivity.class);
                    setResult(Activity.RESULT_OK, intent);
                    break;
            }
        }
    }
}
class DirDetailAdapter extends BaseAdapter{

    Context context;
    LayoutInflater inflater;
    int layout;
    List<AMemo> list = new ArrayList<>();

    DirDetailAdapter(Context context, int layout, List<AMemo> memoList) {
        super();
        this.context = context;
        this.layout = layout;
        list= memoList;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }
    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {return list.get(position).getId();}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_dirdetail, parent, false);
        }

        TextView tvMemo = (TextView)convertView.findViewById(R.id.tvMemo);
        tvMemo.setText(list.get(position).getText());

        TextView tvMemoDetail = (TextView) convertView.findViewById(R.id.tvMemoDetail);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.memotype);

        switch (list.get(position).getMemoType()){
            case Normal:
                tvMemoDetail.setText("");
                break;
            case Location:
                if(list.get(position).getLocationOption().getAddress().equals("null")){
                    tvMemoDetail.setText(" "); }
                else {tvMemoDetail.setText(list.get(position).getLocationOption().getAddress());}
                imageView.setImageResource(R.drawable.btn_location_on);
                break;
            case Category:
              tvMemoDetail.setText(list.get(position).getCategoryOption().getName());
              imageView.setImageResource(R.drawable.btn_category_on);
                break;
        }
        return convertView;
    }
}