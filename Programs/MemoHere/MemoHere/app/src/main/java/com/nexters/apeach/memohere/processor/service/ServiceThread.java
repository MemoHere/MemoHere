package com.nexters.apeach.memohere.processor.service;


import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Message;

import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.dto.Destination;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.MemoType;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.mapapi.GPSinfo;
import com.nexters.apeach.memohere.search.Item;
import com.nexters.apeach.memohere.search.OnFinishSearchListener;
import com.nexters.apeach.memohere.util.DebugToast;

import java.util.List;

/**
 * Created by shin on 2017. 7. 30..
 */

public class ServiceThread extends Thread implements Runnable {

    private final int SLEEP_TIME = 10 * 1000;

    private DataManager dataManager;

    private final double VELOCITY_THRESHOLD = 15f; //15m/s
    private final double DISTANCE_THRESHOLD = 50f; //50m

    private long prevTime;
    private Context serviceContext;
    private GPSinfo gpsInfo;
    private Location prevLocation;
    private Location memorizedLocation;

    private Handler handler;
    private boolean isRun;

    public ServiceThread(Context context, Handler handler){
        this.handler = handler;

        this.dataManager = DataManager.createInstance(context);

        this.serviceContext = context;
        this.gpsInfo = new GPSinfo(context);
        this.prevTime = System.currentTimeMillis();
    }

    public void stopProc(){

        synchronized (this) {
            this.isRun = false;
        }
    }

    @Override
    public void run(){

//        DebugToast.showOnThread(serviceContext, "service start");
        isRun = true;

        while(isRun){

            long elapsedTime = System.currentTimeMillis() - prevTime;
            this.prevTime = System.currentTimeMillis();

            Location currentLocation = gpsInfo.getLocation();
            if(prevLocation == null){
                prevLocation = currentLocation;
                memorizedLocation = currentLocation;
                continue;
            }

            if(currentLocation != null) {
                if (onAlarm(currentLocation, elapsedTime)) {
                    searchMemoOnDestination(currentLocation);
                }
                else{
                    searchNewMemoOnDestination(currentLocation);
                }
            }

            this.prevLocation.setLatitude(currentLocation.getLatitude());
            this.prevLocation.setLongitude(currentLocation.getLongitude());

            try{
                Thread.sleep(SLEEP_TIME); //10초씩 쉰다.
            }catch (Exception e) {}
        }
    }

    private boolean onAlarm(final Location currentLocation, long elapsedTime){

        boolean rc = false;

        //조건1. 사용자가 10s 동안 50m(자전거 속도) 이내로 움직였는가? => user의 위치와 locationForVelocity 비교
        //이내로 움직인 경우, 조건 2 수행 및 현재 위치 locationForVelocity에 기록
        //더 움직인 경우, 현재 위치만 locationForDistance & locationForVelocity 에 기록
        double currentDistance = currentLocation.distanceTo(prevLocation);
        double currentVelocity = currentDistance * 1000 / elapsedTime;
        double movedDistance = currentLocation.distanceTo(memorizedLocation);

        if(currentVelocity < VELOCITY_THRESHOLD) {

            //조건2. 사용자가 마지막으로 기록한 위치(10s 와 관련 없음)에서 100m를 넘게 움직였는가 => user의 위치와 locationForDistance 비교
            //움직인 경우, locationForDistance에 기록하고 아래 로직을 수행
            //움직이지 않은 경우, 로직을 수행하지 않음
            if(movedDistance > DISTANCE_THRESHOLD) {

                memorizedLocation = currentLocation;
                rc = true;
            }
        }else{

            //너무 빠른 속도로 움직이고 있으므로, 아무 기능 수행 안함
            memorizedLocation = currentLocation;
        }

//        DebugToast.showOnThread(serviceContext, String.format("현재속도: %.2f m/s | 이동거리: %.2f m\n이전대비거리: %.2f m | 측정시간 %.2f s", currentVelocity, currentDistance, movedDistance, elapsedTime / 1000f));
        return rc;
    }

    private void searchNewMemoOnDestination(final Location currentLocation){

        for (RootDirectory dir : dataManager.getDirectories()) {

            //휴지통에 있는 메모리스트는 제외한다
            if(dir.isDeleted())
                continue;

            for (final AMemo memo: dir.getMemoList()){

                if(!memo.onService) {
                    sendMemoMessage(memo, currentLocation);
                    memo.onService = true;
                }
            }
        }
    }

    private void searchMemoOnDestination(final Location currentLocation){

        for (RootDirectory dir : dataManager.getDirectories()) {

            //휴지통에 있는 메모리스트는 제외한다
            if(dir.isDeleted())
                continue;

            for (final AMemo memo: dir.getMemoList()){
                sendMemoMessage(memo, currentLocation);
                memo.onService = true;
            }
        }
    }

    private void sendMemoMessage(final AMemo memo, final Location currentLocation){

        if(memo.getMemoType() == MemoType.Location){
            if(memo.getLocationOption().onDestination(currentLocation)){

                Message msg = handler.obtainMessage();
                msg.obj = memo;

                //쓰레드에 있는 핸들러에게 메세지를 보냄
                handler.sendMessage(msg);
            }
        }

        if(memo.getMemoType() == MemoType.Category){
            memo.getCategoryOption().searchDestination(serviceContext, currentLocation, new OnFinishSearchListener() {

                @Override
                public void onSuccess(List<Item> itemList) {

                    Destination destination = new Destination();
                    destination.setDestinationList(itemList);

                    if(destination.isNearby(currentLocation, memo.getCategoryOption().getRadius())){

                        memo.getCategoryOption().setDestination(destination);

                        Message msg = handler.obtainMessage();
                        msg.obj = memo;

                        //쓰레드에 있는 핸들러에게 메세지를 보냄
                        handler.sendMessage(msg);
                    }
                }

                @Override
                public void onFail() {

                    DebugToast.showOnThread(serviceContext, "category option search fail");
                }
            });
        }
    }
}
