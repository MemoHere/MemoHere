package com.nexters.apeach.memohere.dto.memo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by shin on 2017. 7. 25..
 */

public class RootDirectory {

    private int id;
    private String title;
    private Date createTime;
    private Date updateTime;
    private HashMap<Integer, AMemo> memoMap;
    private boolean deleted;

    public RootDirectory(){

        this.memoMap = new HashMap<>();
        this.createTime = new Date();
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void delete(){
        this.deleted = true;
    }

    public boolean isDeleted(){
        return deleted;
    }

    //handle with memo
    public void addMemo(AMemo memo){

        memo.setParent(this);
        this.memoMap.put(memo.getId(), memo);
    }

    public AMemo getMemo(int id){
        return this.memoMap.get(id);
    }

    public void deleteMemo(int id){
        this.memoMap.remove(id);
    }

    public List<AMemo> getMemoList(){

        return new ArrayList<AMemo>(memoMap.values());
    }

    public void clear(){
        this.memoMap.clear();
    }

    //DataManager에서 디렉토리 내의 메모 존재 유무 확인을 위해 사용
    public boolean containsKeyMemo(int key){
        return memoMap.containsKey(key);
    }
}
