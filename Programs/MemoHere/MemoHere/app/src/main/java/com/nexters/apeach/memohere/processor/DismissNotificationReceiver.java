package com.nexters.apeach.memohere.processor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by shin on 2017. 8. 10..
 */

public class DismissNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

//        MemoNotiManager memoNotiManager = MemoNotiManager.getInstance();
//        if(memoNotiManager == null) {
//            DebugToast.show(context, "Error: service에서 notification manager의 인스턴스가 생성되지 않았습니다");
//            return;
//        }

        Toast.makeText(context, "나중에 다시 알림", Toast.LENGTH_SHORT).show();
    }
}
