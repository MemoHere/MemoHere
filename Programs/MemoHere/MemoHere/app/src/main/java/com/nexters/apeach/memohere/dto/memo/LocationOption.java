package com.nexters.apeach.memohere.dto.memo;

import android.location.Location;

import com.nexters.apeach.memohere.dto.Destination;

import java.io.Serializable;

/**
 * Created by shin on 2017. 8. 6..
 */

public class LocationOption implements Serializable {

    private String title;
    private String address;
    private double longitude;
    private double latitude;
    private String phone;
    private int radius;

    private Destination destination;

    public String getTitle() {  return title;   }

    public void setTitle(String title) {   this.title = title; }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getPhone() {  return phone;   }

    public void setPhone(String phone) {    this.phone = phone; }

    public int getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = (int)radius;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }




    public boolean onDestination(Location currentLocation){

        Location destLocation = new Location("dummy");
        destLocation.setLatitude(this.latitude);
        destLocation.setLongitude(this.longitude);

        return currentLocation.distanceTo(destLocation) <= this.radius;

//        return this.destination.isNearby(currentLocation, this.radius);
    }
}
