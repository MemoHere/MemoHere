package com.nexters.apeach.memohere.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.nexters.apeach.memohere.R;
//import com.example.apeach.memohere.category.SmallCategory;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity {

    public String cateName;
    public String cateDatail;

    public boolean switchflag = false;
    public Switch switchbtn;

    public int itemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_category);


        itemId = getIntent().getIntExtra("category_position",0);

        int[] Cateimg = {R.drawable.btn_cart, R.drawable.btn_spoon, R.drawable.btn_book,R.drawable.btn_cross,R.drawable.btn_camera,R.drawable.btn_cloth,R.drawable.btn_dollar,R.drawable.btn_shop};
        final String[] catename = {"생활용품","식당","도서","의료","디지털/가전","의류/미용","은행","편의시설"};
        final List<Integer> CateImage = new ArrayList<Integer>();
        final List<String> CateName = new ArrayList<>();
        final int[] selecteditem={0,10};
        for(int i=0;i<Cateimg.length;i++){
            CateImage.add(Cateimg[i]);
        }
        for(int i=0;i<catename.length;i++){
            CateName.add(catename[i]);
        }
        final String[][] CateText={ {"대형마트","슈퍼","편의점","잡화","문구"},                                     //생활용품
                                    {"베이커리","레스토랑","패스트푸드","떡집"},                                         //식당
                                    {"서점","도서관"},                                                             //도서
                                    {"약국","이비인후과","내과","소아과","정형외과","안과","피부과","산부인과","치과","한의원"}, //의료
                                    {"SKT","KT","LGU+","폰액세서리"},                                            //디지털/가전
                                    {"남성복","여성복","아동복","화장품","스포츠웨어", "헤어","네일"},                  //의류/미용
                                    {"ATM","KB국민은행","우리은행","신한은행","NH농협은행","KEB하나은행","IBK기업은행"},     //은행
                                    {"세탁소","우체국","열쇠","구두수선","인쇄소"}};                                  //편의시설


        final String[][] CateKeyword= {{"대형마트","슈퍼마켓","다이소","문구"},
                                        {"베이커리","레스토랑","패스트푸드","떡집"},
                                        {"서점","도서관"},
                                        {"약국","이비인후과","내과","소아과","정형외과","안과","피부과","산부인과","치과","한의원"},
                                        {"Tworld","올레대리점","유플러스대리점","폰액세서리"},
                                        {"남성복","여성복","아동복","패션잡화","스포츠웨어","화장품","헤어샵","네일샵"},
                                        {"ATM","KB국민은행","우리은행","신한은행","NH농협은행","KEB하나은행","기업은행"},
                                        {"세탁수선","우체국","열쇠","구두수선","인쇄소"}};


        final List<List<String>> Cate =new ArrayList<List<String>>();
        for(int i=0;i<8;i++){
            List<String> temp =new ArrayList<>();
            for(int j=0;j<CateText[i].length;j++){
                temp.add(CateText[i][j]);
            }
            Cate.add(temp);
        }

        final BigAdapter bigadapter =new BigAdapter();
        final SmallAdapter smallAdapter = new SmallAdapter();

        GridView gv =(GridView)findViewById(R.id.gvCateBig);
        final GridView gridView=(GridView)findViewById(R.id.gvCateSmall);
        final GridView gridViewe=(GridView)findViewById(R.id.gvCateSmallE);
        gv.setAdapter(bigadapter);
        bigadapter.addItemlist(CateImage,CateName);

        ImageView btnSaveCategoty = (ImageView) findViewById(R.id.btnSaveCategory);
        btnSaveCategoty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getStringExtra("whatActivity").equals("add")) {
                    Intent intent = new Intent(CategoryActivity.this, AddDirectoryActivity.class);
                    intent.putExtra("category_position",itemId);
                    //소분류 선택하지 않으면 원래 선택했던 값으로
//                    if(getIntent().getStringExtra("category_detail") !=null && cateDatail == null) {
//                        cateName =getIntent().getStringExtra("category_name");
//                        cateDatail = getIntent().getStringExtra("category_detail");
                    if(cateName == null || cateDatail == null){
                        Toast.makeText(CategoryActivity.this,"소분류를 선택해주세요.",Toast.LENGTH_SHORT).show();
                    }
                    else{
                    intent.putExtra("category_name",cateName);
                    intent.putExtra("category_detail",cateDatail);
                    intent.putExtra("switch",switchflag);
//                    categoryOption.setRadius(data.getDoubleExtra("category_radius",0));
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                    }
                }


                else if(getIntent().getStringExtra("whatActivity").equals("update")) {
                    Intent intent = new Intent(CategoryActivity.this, UpdateDirectoryActivity.class);
                    intent.putExtra("category_position",itemId);
                    if(cateName == null || cateDatail == null){
                        Toast.makeText(CategoryActivity.this,"소분류를 선택해주세요.",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        intent.putExtra("category_name",cateName);
                        intent.putExtra("category_detail",cateDatail);
//                    categoryOption.setRadius(data.getDoubleExtra("category_radius",0));
                        intent.putExtra("switch",switchflag);
                        intent.setAction(Long.toString(SystemClock.currentThreadTimeMillis()));
                        setResult(Activity.RESULT_OK,intent);
                        finish();
                    }

                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }
        });


        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int position, long l) {
                cateName = CateName.get(position);
                cateDatail=null;
                if(selecteditem[0]!=position) {
                    smallAdapter.clearItem();
                    if (position == 3) {
                        gridViewe.setVisibility(View.GONE);
                        smallAdapter.setTag(1);
                        gridView.setAdapter(smallAdapter);
                        gridView.setVisibility(View.VISIBLE);

                    } else {
                        gridView.setVisibility(View.GONE);
                        gridViewe.setAdapter(smallAdapter);
                        smallAdapter.setTag(0);
                        gridViewe.setVisibility(View.VISIBLE);
                    }
                    selecteditem[0]=position;
                    selecteditem[1]=10;
                    for(int i=0;i<Cate.get(selecteditem[0]).size();i++){
                        smallAdapter.selected[i]=0;
                    }
                    for(int i=0;i<8;i++){
                        bigadapter.selected[i]=0;
                    }
                    bigadapter.selected[position]=1;
                    bigadapter.notifyDataSetChanged();

                    for (int i = 0; i < Cate.get(position).size(); i++) {
                        smallAdapter.addItem(Cate.get(position).get(i));
                    }
                    smallAdapter.notifyDataSetChanged();
                }
            }
        });
        gridViewe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int position, long l) {
                if(selecteditem[1]!=position&&position<Cate.get(selecteditem[0]).size()){
                    for(int i=0;i<Cate.get(selecteditem[0]).size();i++){
                        smallAdapter.selected[i]=0;
                    }
                    cateDatail = Cate.get(selecteditem[0]).get(position);
                    smallAdapter.selected[position]=1;
                    smallAdapter.notifyDataSetChanged();
                    selecteditem[1]=position;
                }
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int position, long l) {
                if(selecteditem[1]!=position&&position<Cate.get(selecteditem[0]).size()){
                    for(int i=0;i<Cate.get(selecteditem[0]).size();i++){
                        smallAdapter.selected[i]=0;
                    }
                    cateDatail = Cate.get(selecteditem[0]).get(position);
                    smallAdapter.selected[position]=1;
                    smallAdapter.notifyDataSetChanged();
                    selecteditem[1]=position;
                }
            }
        });
        cateName = getIntent().getStringExtra("category_name");
        cateDatail = getIntent().getStringExtra("category_detail");
        int cateIdx =-1;
        //이미 선택된 카테고리가 있으면 세팅
        if( cateName != null){ //BigCatetgory
            cateIdx = CateName.indexOf(cateName);
            bigadapter.selected[cateIdx]=1;
            bigadapter.notifyDataSetChanged();
            selecteditem[0]=cateIdx;

            if (cateIdx == 3) {
                gridViewe.setVisibility(View.GONE);
                smallAdapter.setTag(1);
                gridView.setAdapter(smallAdapter);
                gridView.setVisibility(View.VISIBLE);

            } else {
                gridView.setVisibility(View.GONE);
                gridViewe.setAdapter(smallAdapter);
                smallAdapter.setTag(0);
                gridViewe.setVisibility(View.VISIBLE);
            }
            for (int i = 0; i < Cate.get(cateIdx).size(); i++) {
                smallAdapter.addItem(Cate.get(cateIdx).get(i));
            }
            smallAdapter.notifyDataSetChanged();
        }else{
            cateName="생활용품"; cateDatail="대형마트";
            bigadapter.selected[0]=1;
            bigadapter.notifyDataSetChanged();
            selecteditem[0]=0;

            gridView.setVisibility(View.GONE);
            gridViewe.setAdapter(smallAdapter);
            smallAdapter.setTag(0);
            gridViewe.setVisibility(View.VISIBLE);

            for (int i = 0; i < Cate.get(0).size(); i++) {
                smallAdapter.addItem(Cate.get(0).get(i));
            }
            smallAdapter.notifyDataSetChanged();
            selecteditem[1]=0;
            smallAdapter.selected[0]=1;
            smallAdapter.notifyDataSetChanged();
        }
        if( cateDatail != null && cateIdx!=-1){ //SmallCatetgory
            int cateIndex = Cate.get(cateIdx).indexOf(cateDatail);
            selecteditem[1]=cateIndex;
            smallAdapter.selected[cateIndex]=1;
            smallAdapter.notifyDataSetChanged();
        }


        switchbtn = (Switch)findViewById(R.id.switch2);
        switchbtn.setChecked(!getIntent().getBooleanExtra("switch",false));
        switchflag = getIntent().getBooleanExtra("switch",false);
        switchbtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    switchflag = false;
                }
                else{
                    switchflag = true;

                    }
            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(getIntent().getStringExtra("whatActivity").equals("add")) {
            Intent intent = new Intent(CategoryActivity.this, AddDirectoryActivity.class);
            intent.putExtra("category_position",itemId);
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        }

        else if(getIntent().getStringExtra("whatActivity").equals("update")) {
            Intent intent = new Intent(CategoryActivity.this, UpdateDirectoryActivity.class);
            intent.putExtra("category_position",itemId);
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
        }
    }

}

class BigAdapter extends BaseAdapter{

    public List<Integer> ItemList = new ArrayList<Integer>();
    public List<String>SItemList =new ArrayList<String>();
    public int selected[]={0,0,0,0,0,0,0,0};

    @Override
    public int getCount() {
        return ItemList.size();
    }

    @Override
    public Object getItem(int pos) {
        return ItemList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        final int position = pos;

        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.categorybig,parent,false);
        }
        ConstraintLayout cl = (ConstraintLayout)convertView.findViewById(R.id.clBigCate);
        ImageView iv = (ImageView)convertView.findViewById(R.id.ivCate);
        TextView tv = (TextView)convertView.findViewById(R.id.tvBigCate);
        if(selected[pos]==1) {
            cl.setBackgroundColor(Color.rgb(216, 216, 216));
            iv.setBackgroundColor(Color.rgb(216, 216, 216));
            tv.setBackgroundColor(Color.rgb(216, 216, 216));
        }
        else {
            cl.setBackgroundColor(Color.rgb(243, 243, 243));
            iv.setBackgroundColor(Color.rgb(243, 243, 243));
            tv.setBackgroundColor(Color.rgb(243, 243, 243));
        }
        iv.setImageResource(ItemList.get(position));
        tv.setText(SItemList.get(position));
        return convertView;
    }

    public void addItemlist(List<Integer> BigCate,List<String> SBigCate){
        this.ItemList.addAll(BigCate);
        this.SItemList.addAll(SBigCate);
    }

}

class SmallAdapter extends BaseAdapter{

    public List<SmallCategory> ItemList = new ArrayList<SmallCategory>();
    int tag;
    public int selected[]={0,0,0,0,0,0,0,0,0,0};


    @Override
    public int getCount() {
       return ItemList.size();
    }

    @Override
    public Object getItem(int pos) {
     return ItemList.get(pos);
    }

    public void setTag(int tag){
        this.tag=tag;
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }
    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        final int position = pos;


        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if(tag==1)
                convertView=inflater.inflate(R.layout.categorysmalle,parent,false);
            else
                convertView=inflater.inflate(R.layout.categorysmall,parent,false);
        }
        TextView tv = (TextView)convertView.findViewById(R.id.tvSmallCate);
        SpannableStringBuilder string =new SpannableStringBuilder(ItemList.get(position).keyword);
        if(selected[pos]==1){
            string.setSpan(new UnderlineSpan(),0,string.length(),0);
            tv.setTextColor(Color.parseColor("#37447a"));
        }else{
            tv.setTextColor(Color.parseColor("#FFFFFF"));
        }

        tv.setText(string);

        return convertView;
    }

    public void clearItem(){
        ItemList.clear();
    }

    public void addItem(String input){
        SmallCategory smallCategory = new SmallCategory();
        smallCategory.keyword=input;
        ItemList.add(smallCategory);
    }
}
class SmallCategory{
    public String keyword= new String();

    public void SetKeword(String input){
        this.keyword=input;
    }

    public String GetKeword(){
        return this.keyword;
    }
}


