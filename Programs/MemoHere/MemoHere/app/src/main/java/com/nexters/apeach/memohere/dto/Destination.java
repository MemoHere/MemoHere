package com.nexters.apeach.memohere.dto;

import android.location.Location;

import com.nexters.apeach.memohere.search.Item;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shin on 2017. 7. 25..
 */

public class Destination implements Serializable{

    private List<Item> destinationList;

    public List<Item> getItemList() {
        return destinationList;
    }

    public void setDestinationList(List<Item> destinationList) {
        this.destinationList = destinationList;
    }



    public boolean isNearby(Location currentLocation, double radius) {

        if(destinationList != null && destinationList.size() > 0) {

            for (Item destination : destinationList) {

                Location destLocation = new Location("dummy");
                destLocation.setLatitude(destination.latitude);
                destLocation.setLongitude(destination.longitude);

                if (currentLocation.distanceTo(destLocation) <= radius)
                    return true;
            }
        }

        return false;
    }
}
