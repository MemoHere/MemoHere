package com.nexters.apeach.memohere.dto.memo;

import android.content.Context;
import android.location.Location;

import com.nexters.apeach.memohere.dto.Destination;
import com.nexters.apeach.memohere.search.OnFinishSearchListener;
import com.nexters.apeach.memohere.search.Searcher;

import java.io.Serializable;

/**
 * Created by shin on 2017. 8. 6..
 */

public class CategoryOption implements Serializable{

    private String name;
    private String details;
    private String[] keywords;
    private int radius = 400; //400m

    private Destination destination;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getRadius() {
        return radius;
    }

//    public void setRadius(double radius) {
//        this.radius = (int)radius;
//    }

    public Destination getDestination() {
        return destination;
    }

    public String[] getKeywords() {
        return keywords;
    }

    public void setKeywords(String[] keywords) {
        this.keywords = keywords;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }


    public void searchDestination(Context context, Location currentLocation, OnFinishSearchListener onFinishSearchListener){

        Searcher searcher = new Searcher();
        String query = details;
        int rad = this.radius; // 중심 좌표부터의 반경거리. 특정 지역을 중심으로 검색하려고 할 경우 사용. meter 단위 (0 ~ 10000)
        int page = 1;

        if(query.equals("잡화")) query = "다이소";
        if(query.equals("SKT")) query = "Tworld";
        if(query.equals("KT")) query = "올레대리점";
        if(query.equals("LGU+")) query = "유플러스대리점";
        searcher.searchDestination(context.getApplicationContext(), query, currentLocation, rad, page, onFinishSearchListener);
    }
}
