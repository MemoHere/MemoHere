package com.nexters.apeach.memohere.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nexters.apeach.memohere.R;

/*
 * Created by Jiwon on 2017-07-19
 * Simple activity for running splash activity and initializing project(loading page).
 */

public class InitialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        startActivity(new Intent(this, SplashActivity.class));
        InitialActivity.this.finish();
    }
}


