package com.nexters.apeach.memohere.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.nexters.apeach.memohere.R;
import com.nexters.apeach.memohere.dao.DataManager;
import com.nexters.apeach.memohere.dto.memo.AMemo;
import com.nexters.apeach.memohere.dto.memo.RootDirectory;
import com.nexters.apeach.memohere.mapapi.GPSinfo;
import com.nexters.apeach.memohere.mapapi.MapApiConst;
import com.nexters.apeach.memohere.processor.MemoNotiManager;
import com.nexters.apeach.memohere.search.Item;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import java.util.List;

/**
 * Created by shin on 2017. 8. 17..
 */

public class NotificationDeatilsActivity extends AppCompatActivity implements MapView.MapViewEventListener, MapView.POIItemEventListener{

    private MapView mMapView;
    private DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notification_details);
        mMapView = (MapView)findViewById(R.id.memo_details_mapview);
        mMapView.setDaumMapApiKey(MapApiConst.DAUM_MAPS_ANDROID_APP_API_KEY);
        mMapView.setMapViewEventListener(this);
        mMapView.setPOIItemEventListener(this);

        final TextView tvPhone = (TextView)findViewById(R.id.memo_details_dest_phone);
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = tvPhone.getText().toString();
                str="tel:"+str.replace("-","");
                Intent phintent = new Intent("android.intent.action.DIAL", Uri.parse(str));
                startActivity(phintent);
            }
        });

        dataManager = DataManager.createInstance(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        MemoNotiManager notiManager = MemoNotiManager.createInstance(getSystemService(NOTIFICATION_SERVICE));
//
//        AMemo aMemo = new AMemo();
//        aMemo.setText("우유 사기");
//        aMemo.setMemoType(MemoType.Normal);
//
//        RootDirectory dir = new RootDirectory();
//        dir.setTitle("편의점");
//        dir.addMemo(aMemo);
//
//        if(dataManager.getDirectory(dir.getId()) == null)
//            dataManager.addDirectory(dir);
//
//        notiManager.notifyCustom(this, aMemo);
//
//        DebugToast.show(this, "back pressed");
    }

    private void createUser(int radius){

        mMapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithMarkerHeadingWithoutMapMoving);
        mMapView.setDefaultCurrentLocationMarker();
        mMapView.setShowCurrentLocationMarker(true);
        mMapView.setCurrentLocationRadius(radius);
//        mMapView.setCurrentLocationRadiusFillColor(Color.green(128));
//        mMapView.setCurrentLocationRadiusStrokeColor(Color.red(128));
    }

    private MapPOIItem createDestinationPOIItem(Item destination){

        MapPoint mapPoint = MapPoint.mapPointWithGeoCoord(destination.latitude, destination.longitude);

        MapPOIItem marker = new MapPOIItem();
        marker.setItemName(destination.title);
        marker.setUserObject(destination);
        marker.setMapPoint(mapPoint);
        marker.setMarkerType(MapPOIItem.MarkerType.BluePin);
        marker.setSelectedMarkerType(MapPOIItem.MarkerType.RedPin);

        return marker;
    }

    private void readBundle(Bundle bundle){

        int dirID = bundle.getInt(MemoNotiManager.EXTRA_DIR_KEY);
        int memoID = bundle.getInt(MemoNotiManager.EXTRA_MEMO_KEY);

        RootDirectory dir = dataManager.getDirectory(dirID);
        AMemo memo = dir.getMemo(memoID);

        String title = "근처에 [" + memo.getUserTitle(15)+ "] 이 있습니다";
        String content = "\"" + memo.getText() + "\"";

        List<Item> items = memo.getOptionDestinationList();
        this.setDestinationItems(items);
        this.createUser(memo.getOptionRadius());

        TextView tv = (TextView)findViewById(R.id.memo_details_title);
        tv.setText(title);
        tv = (TextView)findViewById(R.id.memo_details_content);
        tv.setText(content);
    }

    private void setDestinationItems(List<Item> items){

        if(items != null) {
            //옵션에 있던 목적지 추가
            mMapView.removeAllPOIItems();
            for (Item item : items) {
                mMapView.addPOIItem(createDestinationPOIItem(item));
            }

            //추가된 목적지 중에 첫번째 아이템 선택
            MapPOIItem[] poiItems = mMapView.getPOIItems();
            if(poiItems != null && poiItems.length > 0){
                mMapView.selectPOIItem(poiItems[0], false);
                this.onPOIItemSelected(mMapView, poiItems[0]);
            }
        }
    }

    @Override
    public void onMapViewInitialized(MapView mapView) {

        GPSinfo gpsinfo = new GPSinfo(this);
        MapPoint mapPoint = MapPoint.mapPointWithGeoCoord(gpsinfo.getLatitude(),gpsinfo.getLongitude());
        mMapView.setMapCenterPointAndZoomLevel(mapPoint, 3, false);
        mMapView.removeAllPOIItems();


        //receive notification's intent
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            this.readBundle(bundle);
        }
    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {

    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {

//        Searcher searcher = new Searcher();
//        Location location = new Location("dummy");
//        location.setLongitude(mapPoint.getMapPointGeoCoord().longitude);
//        location.setLatitude(mapPoint.getMapPointGeoCoord().latitude);
//
//        searcher.searchDestination(this, "베르가모", location, 1000, 1, new OnFinishSearchListener() {
//
//            @Override
//            public void onSuccess(List<Item> itemList) {
//
//                Destination destination = new Destination();
//                destination.setDestinationList(itemList);
//
//                if(destination.getItemList() != null) {
//                    for (Item item : destination.getItemList()) {
//                        mMapView.addPOIItem(createDestinationPOIItem(item));
//                    }
//                }
//            }
//
//            @Override
//            public void onFail() {
//
//            }
//        });
    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {

    }



    @Override
    public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {

        Item item = (Item)mapPOIItem.getUserObject();

        TextView tv = (TextView)findViewById(R.id.memo_details_dest_name);
        tv.setText(item.title);

        tv = (TextView)findViewById(R.id.memo_details_dest_address);
        tv.setText(item.address);

        tv = (TextView)findViewById(R.id.memo_details_dest_phone);
        tv.setText(item.phone);
    }

    @Override
    @Deprecated
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {

    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {

    }

    @Override
    public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {

    }
}
