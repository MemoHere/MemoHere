package com.nexters.apeach.memohere.processor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by shin on 2017. 8. 12..
 */

public class CancelNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        int memoId = intent.getIntExtra(MemoNotiManager.EXTRA_MEMO_KEY, -1);

        MemoNotiManager notiManager = MemoNotiManager.getInstance();
        if(notiManager == null) {
            Toast.makeText(context, "서비스가 동작 중이 아닙니다", Toast.LENGTH_SHORT).show();
            return;
        }
        notiManager.cancelNotification(memoId);
        notiManager.addNoAlarmList(memoId);

        Toast.makeText(context, "오늘 하루 알림이 꺼졌습니다", Toast.LENGTH_SHORT).show();
    }
}
