package memohere.example.DTO.Memo.test;

/**
 * Created by shin on 2017. 7. 24..
 */

public abstract class Memo {
    protected int idx;
    protected String title;
    protected String message;

    public Memo(int idx) {
        this.idx = idx;
    }

    public int getIdx() {
        return idx;
    }

    public abstract void setIdx(int idx);

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
