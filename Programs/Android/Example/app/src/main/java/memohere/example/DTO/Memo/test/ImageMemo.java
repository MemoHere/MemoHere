package memohere.example.DTO.Memo.test;

/**
 * Created by shin on 2017. 7. 24..
 */

public class ImageMemo extends Memo {
    private byte[] image;



    public ImageMemo(int idx) {
        super(idx);
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
