package memohere.example.DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shin on 2017. 7. 18..
 */

public interface IDestination {

    List<Location> getLocationList();
    double getRadius();
    boolean isStandingOn(double lon, double lat);
}
