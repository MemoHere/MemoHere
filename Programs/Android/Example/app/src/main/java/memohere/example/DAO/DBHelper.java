package memohere.example.DAO;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;

/**
 * Created by shin on 2017. 7. 18..
 */

public class DBHelper extends SQLiteOpenHelper {


    public DBHelper(String dbPath){
        super(null, dbPath, null, 0);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
