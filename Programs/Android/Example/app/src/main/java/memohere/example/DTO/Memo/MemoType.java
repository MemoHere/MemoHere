package memohere.example.DTO.Memo;

/**
 * Created by shin on 2017. 7. 18..
 */

public enum MemoType {
    Normal,
    Location,
    Category
}
