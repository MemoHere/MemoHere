package memohere.example.DTO;

/**
 * Created by shin on 2017. 7. 18..
 */

public class Location {
    public double Logitude;
    public double Latitude;

    public String Name;
    public String Address;
}
