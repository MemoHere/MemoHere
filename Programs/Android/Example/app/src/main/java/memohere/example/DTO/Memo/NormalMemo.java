package memohere.example.DTO.Memo;

import java.util.Date;

/**
 * Created by shin on 2017. 7. 18..
 */

public class NormalMemo implements IMemo {

    public NormalMemo(){

    }

    @Override
    public long getID() {
        return 0;
    }

    @Override
    public MemoType getType() {
        return MemoType.Normal;
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public Date getCreationTime() {
        return null;
    }

    @Override
    public Date getUpdateTime() {
        return null;
    }

    @Override
    public boolean isDeleted() {
        return false;
    }

    @Override
    public boolean isVisible() {
        return false;
    }
}
