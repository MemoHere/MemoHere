package memohere.example.DTO.Memo;

import java.util.Date;

/**
 * Created by shin on 2017. 7. 18..
 */

public interface IMemo {

    long getID();
    MemoType getType();
    String getText();
    Date getCreationTime();
    Date getUpdateTime();
    boolean isDeleted();
    boolean isVisible();
}
