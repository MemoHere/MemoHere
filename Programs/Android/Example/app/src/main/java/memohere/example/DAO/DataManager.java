package memohere.example.DAO;

import android.provider.ContactsContract;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import memohere.example.DTO.Memo.IMemo;
import memohere.example.DTO.MemoUser;

/**
 * Created by shin on 2017. 7. 18..
 */

public class DataManager {

    //DB Connection Default Info
    private final String dbPath = "documents/MemoHere.db";
    private DBHelper dbHelper = null;

    //Logical Memo Data
    private HashMap<Long, IMemo> memoHashMap;
    //User Info
    private MemoUser user;


    public DataManager(){

    }


    //Handle with db connection
    public boolean Connect(){

        return false;
    }

    public void Close(){
        dbHelper.close();
        dbHelper = null;
    }


    //Handle with Memo
    public void Add(IMemo memo){

    }

    public void Delete(int id){

    }

    public void Update(IMemo memo){

    }

    public final List<IMemo> getMemoWithNormal(){

        return null;

//        아직 사용하기에 버전이 불안정함
//        List<IMemo> memos = memoHashMap.entrySet().stream()
//                .filter(map -> !map.getValue().isDeleted() && map.getValue().isVisible())
//                .map(m -> m.getValue())
//                .collect(Collectors.toList());
    }

    public final List<IMemo> getDeletedMemo(){
        return null;
    }

    public final List<IMemo> getHidingMemo(){
        return null;
    }


    //Handle with User Info
    public MemoUser getUser(){
        return user;
    }
}
