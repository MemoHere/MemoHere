package memohere.example.DTO.Memo;

import java.util.Date;

import memohere.example.DTO.IDestination;
import memohere.example.DTO.Location;

/**
 * Created by shin on 2017. 7. 18..
 */

public class CategoryMemo implements IMemo {

    private String categoryName;
    private String categryDetails;
    private Location baseLocation;
    private IDestination destination;

    @Override
    public long getID() {
        return 0;
    }

    @Override
    public MemoType getType() {
        return MemoType.Category;
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public Date getCreationTime() {
        return null;
    }

    @Override
    public Date getUpdateTime() {
        return null;
    }

    @Override
    public boolean isDeleted() {
        return false;
    }

    @Override
    public boolean isVisible() {
        return false;
    }



    //check onDestination
    public void setDestination(IDestination destination){
        this.destination = destination;
    }

    public boolean onDestination(double lon, double lat){
        return this.destination.isStandingOn(lon, lat);
    }
}
