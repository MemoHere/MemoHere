package memohere.example.DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shin on 2017. 7. 18..
 */

public class Destination implements IDestination{


    @Override
    public List<Location> getLocationList() {
        return null;
    }

    @Override
    public double getRadius() {
        return 0;
    }

    @Override
    public boolean isStandingOn(double lon, double lat) {
        return false;
    }
}
